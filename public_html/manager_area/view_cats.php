﻿<?php
session_start();
if(($_SESSION['level'] != "manager")){
	echo "<script>window.open('login.php?not_manager=Jūs ne vadybininkas!','_self')</script>";
}
else{
?>
<table width="795" align="center" bgcolor="pink">
	<tr align="center">
		<td colspan="6"><h2>Peržiūrėti visas kategorijas</h2></td>
	</tr>
	<tr align="center" bgcolor="orange">
		<th>ID</th>
		<th>Pavadinimas</th>
		<th>Redaguoti</th>
		<th>Ištrinti</th>
	</tr>
	<?php
		include("includes/db.php");
		$get_cat = "select * from categories";
		$run_cat = mysqli_query($con,$get_cat);
		$i = 0;
		while($row_cat=mysqli_fetch_array($run_cat)){
			$cat_id = $row_cat['cat_id'];
			$cat_title = $row_cat['cat_title'];
			$i++;
	?>
	<tr align="center">
		<td><?php echo $i;?></td>
		<td><?php echo $cat_title;?></td>
		<td><a href="index.php?edit_cat=<?php echo $cat_id;?>">Redaguoti</a></td>
		<td><a href="delete_cat.php?delete_cat=<?php echo $cat_id;?>">Ištrinti</a></td>
	</tr>
		<?php } ?>
</table>
<?php } ?>