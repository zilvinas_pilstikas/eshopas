﻿<?php
if(($_SESSION['level'] != "vadybininkas")){
	echo "<script>window.open('login.php?not_manager=Jūs ne vadybininkas!','_self')</script>";
}
else{
?>
<?php
include("includes/db.php");
if(isset($_GET['edit_warehouse'])){
	$warehouse_id = $_GET['edit_warehouse'];
	$get_warehouse = "select * from sandelis where sandelio_id='$warehouse_id'";
	$run_warehouse = mysqli_query($con,$get_warehouse);
	$row_warehouse = mysqli_fetch_array($run_warehouse);
	$warehouse_city = $row_warehouse['miestas'];
	$warehouse_address = $row_warehouse['adresas'];
	$warehouse_limit = $row_warehouse['talpa'];
	$warehouse_qty = $row_warehouse['kiekis'];
	$warehouse_state = $row_warehouse['busena'];
	$warehouse_mobile = $row_warehouse['telefonas'];
}
?>
<form action="" method="post">
	<table align="center" width="795" border="2" bgcolor="orange">
		<tr align="center">
			<td colspan="7"><h2>Atnaujinti sandėlį</h2></td>
		</tr>
		
		<tr>
			<td align="right"><b>Miestas:</b></td>
			<td><input type="text" name="warehouse_city" size ="40" value="<?php echo $warehouse_city;?>" required /></td>
		</tr>
		
		<tr>
			<td align="right"><b>Adresas:</b></td>
			<td><input type="text" name="warehouse_address" size ="40" value="<?php echo $warehouse_address;?>" required /></td>
		</tr>
		
		<tr>
			<td align="right"><b>Talpa:</b></td>
			<td><input type="text" name="warehouse_limit" size ="40" value="<?php echo $warehouse_limit;?>" required /></td>
		</tr>
		
		<tr>
			<td align="right"><b>Telefonas:</b></td>
			<td><input type="text" name="warehouse_mobile" size ="40" value="<?php echo $warehouse_mobile;?>" required /></td>
		</tr>
		
		<tr align="center">
			<td colspan="7"><input type="submit" name="update_post" value="Atnaujinti sandėlį" /></td>
		</tr>
			
	</table>
</form>

<?php
	if(isset($_POST['update_post'])){
	$city = $_POST['warehouse_city'];
	$address = $_POST['warehouse_address'];
	$limit = $_POST['warehouse_limit'];
	$mobile = $_POST['warehouse_mobile'];
	
	if ($warehouse_qty > $limit){
		echo "<script>alert('Sandėlio nepavyko atnaujinti! Talpa yra per maža!')</script>";
		exit();
	}
	
	if ($warehouse_qty == $limit){
		$warehouse_state = "Pilnas";
	} else if ($warehouse_qty == 0){
		$warehouse_state = "Tuscias";
	} else{
		$warehouse_state = "Aktyvus";
	}
	
	$update_warehouse = "update sandelis set miestas='$city', adresas='$address', talpa='$limit', busena='$warehouse_state', telefonas='$mobile' where sandelio_id='$warehouse_id'";
	$run_update = mysqli_query($con, $update_warehouse);
	if($run_update){
		echo "<script>alert('Sandėlys buvo sėkmingai atnaujintas')</script>";
		echo "<script>window.open('index.php?view_warehouse','_self')</script>";
	}
	}
?>
<?php } ?>