﻿<?php
session_start();
if(($_SESSION['level'] != "vadybininkas")){
	echo "<script>window.open('login.php?not_manager=Jūs ne vadybininkas!','_self')</script>";
}
else{
?>
<!DOCTYPE>


<html>
	<head>
		<title>Produktų įterpimas</title>
		
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script>tinymce.init({ selector:'textarea' });</script>
	
	</head>
	
<body bgcolor="skyblue">
	<form action="insert_product.php" method="post" enctype="multipart/form-data">
		<table align="center" width="795" border="2" bgcolor="orange">
			<tr align="center">
				<td colspan="7"><h2>Įterpti naują produktą</h2></td>
			</tr>
			
			<tr>
				<td align="right"><b>Produkto brūkšninis kodas:</b></td>
				<td><input type="text" name="product_bar_code" size ="60" required /></td>
			</tr>
			
			<tr>
				<td align="right"><b>Produkto pavadinimas:</b></td>
				<td><input type="text" name="product_title" size ="60" required /></td>
			</tr>
			
		
			<tr>
				<td align="right"><b>Produkto gamintojas:</b></td>
				<td><input type="text" name="product_brand" size ="60" required /></td>
			</tr>
			
			<tr>
				<td align="right"><b>Produkto kategorija:</b></td>
				<td><input type="text" name="product_cat" size ="60" required /></td>
			</tr>
			
			<tr>
				<td align="right"><b>Produkto kaina:</b></td>
				<td><input type="text" name="product_price" required /></td>
			</tr>
			
			<tr>
				<td align="right"><b>Produkto kiekis:</b></td>
				<td><input type="text" name="product_qty" required /></td>
			</tr>
			
			<tr>
				<td align="right"><b>Produkto nuotrauka:</b></td>
				<td><input type="file" name="product_image" required /></td>
			</tr>
			
			<tr>
				<td align="right"><b>Produkto Aprašymas:</b></td>
				<td><textarea id="TextArea" name="product_desc" cols="20" rows="10"></textarea></td>
			</tr>
			
			<tr>
				<td align="right"><b>Produkto raktažodžiai:</b></td>
				<td><input type="text" name="product_keywords" size="50" required/></td>
			</tr>
			
			<tr>
				<td align="right"><b>Produkto sandėlis:</b></td>
				<td>
					<select name="product_warehouse" required>
						<option value="" disabled>Pasirinkti sandėlį</option>
						<?php
							$get_warehouse = "select * from sandelis";
							$run_warehouse = mysqli_query($con,$get_warehouse);
							while($row_warehouse = mysqli_fetch_array($run_warehouse)){
								$warehouse_id = $row_warehouse['sandelio_id'];
								$warehouse_title = $row_warehouse['adresas'];
								echo "<option value='$warehouse_id'>$warehouse_title</option>";
							}
						?>
					</select>
				</td>
			</tr>
			
			<tr align="center">
				<td colspan="7"><input type="submit" name="insert_post" value="Įterpti produktą" /></td>
			</tr>
			
		</table>
	</form>

</body>
</html>

<?php
include("includes/db.php");
	if(isset($_POST['insert_post'])){
		//Gaunami tekstiniai duomenys is laukuj
		$product_bar_code = $_POST['product_bar_code'];
		$product_title = $_POST['product_title'];
		$product_cat = $_POST['product_cat'];
		$product_brand = $_POST['product_brand'];
		$product_price = $_POST['product_price'];
		$product_qty = $_POST['product_qty'];
		$product_desc = $_POST['product_desc'];
		$product_keywords = $_POST['product_keywords'];
		$product_warehouse = $_POST['product_warehouse'];
		
		//Gaunamas paveiksliukas is lauko
		$product_image = $_FILES['product_image']['name'];
		$product_image_tmp = $_FILES['product_image']['tmp_name'];
		move_uploaded_file($product_image_tmp,"product_images/$product_image");
		
		$select_warehouse = "select * from sandelis where sandelio_id='$product_warehouse'";
		$run_warehouse = mysqli_query($con,$select_warehouse);
		$row_warehouse = mysqli_fetch_array($run_warehouse);
		$warehouse_qty = $row_warehouse['kiekis'];
		$warehouse_limit = $row_warehouse['talpa'];
		$warehouse_busena = $row_warehouse['busena'];
		
		$warehouse_new_qty = $warehouse_qty + $product_qty;
		
		if ($warehouse_new_qty <= $warehouse_limit){
		
			$insert_product = "insert into prekes (bruksninis_kodas,pavadinimas,gamintojas,nuotrauka,kategorija,aprasymas,raktazodziai,kaina,kiekis,FK_sandelio_id) 
						values ('$product_bar_code','$product_title','$product_brand','$product_image','$product_cat','$product_desc',
									'$product_keywords','$product_price','$product_qty','$product_warehouse')"; 														
			$insert_pro = mysqli_query($con,$insert_product);
			if($insert_pro){
				
				if ($warehouse_new_qty == $warehouse_limit){
					$warehouse_busena = "Pilnas";
				} else if ($warehouse_busena != "Aktyvus" && $warehouse_new_qty != 0) {
					$warehouse_busena = "Aktyvus";
				}
				$update_qty = "update sandelis set kiekis='$warehouse_new_qty', busena='$warehouse_busena' where sandelio_id='$product_warehouse'";
				$run_update = mysqli_query($con,$update_qty);
				
				
				echo "<script>alert('Produktas buvo sėkmingai įterptas')</script>";
				echo "<script>window.open('index.php?insert_product','_self')</script>";
		}
		}
	}
?>
<?php } ?>