﻿<?php
session_start();
if(($_SESSION['level'] != "vadybininkas")){
	echo "<script>window.open('login.php?not_manager=Jūs ne vadybininkas!','_self')</script>";
}
else{
?>
<table width="795" align="center" bgcolor="pink">
	<tr align="center">
		<td colspan="6"><h2>Peržiūrėti visus produktus</h2></td>
	</tr>
	<tr align="center" bgcolor="orange">
		<th>S.N</th>
		<th>Brūkšninis kodas</th>
		<th>Pavadinimas</th>
		<th>Nuotrauka</th>
		<th>Kaina</th>
		<th>Kiekis</th>
		<th>Nupirkta</th>
		<th>Redaguoti</th>
	</tr>
	<?php
		include("includes/db.php");
		$get_pro = "select * from prekes";
		$run_pro = mysqli_query($con,$get_pro);
		$i = 0;
		while($row_pro=mysqli_fetch_array($run_pro)){
			$pro_id = $row_pro['bruksninis_kodas'];
			$pro_title = $row_pro['pavadinimas'];
			$pro_image = $row_pro['nuotrauka'];
			$pro_price = $row_pro['kaina'];
			$pro_qty = $row_pro['kiekis'];
			$pro_sold = $row_pro['nupirkta'];
			$i++;
	?>
	<tr align="center">
		<td><?php echo $i;?></td>
		<td><?php echo $pro_id;?></td>
		<td><?php echo $pro_title;?></td>
		<td><img src="product_images/<?php echo $pro_image;?>" width="60" height="60"/></td>
		<td><?php echo $pro_price;?></td>
		<td><?php echo $pro_qty;?></td>
		<td><?php echo $pro_sold;?></td>
		<td><a href="index.php?edit_pro=<?php echo $pro_id;?>">Redaguoti</a></td>
	</tr>
		<?php } ?>
</table>
<?php } ?>