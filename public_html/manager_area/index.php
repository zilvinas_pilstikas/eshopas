﻿<?php
include("includes/db.php");
session_start();
if(($_SESSION['level'] != "vadybininkas")){
	echo "<script>window.open('login.php?not_manager=Jūs ne vadybininkas!','_self')</script>";
}
else{
?>
<!DOCTYPE>
<html>
	<head>
		<title>Čia yra vadybininko skyrius</title>
		<link rel="stylesheet" href="styles/style.css" media="all"/>
	</head>
<body>
	<div class="main_wrapper">
		<div id="header"></div>
		<div id="right">
			<h2 style="text-align:center;">Valdyti turinį</h2>
			<a href="index.php?insert_product">Įterpti naują produktą</a>
			<a href="index.php?view_products">Peržiūrėti visus produktus</a>
			<a href="index.php?insert_warehouse">Įterpti naują sandėlį</a>
			<a href="index.php?view_warehouse">Peržiūrėti visus sandėlius</a>
			<a href="index.php?finance">Finansų ataskaita</a>
			<a href="logout.php">Atsijungti</a>
		</div>
		<div id="left">
		<h2 style="color:red; text-align:center;"><?php echo @$_GET['logged_in']; ?></h2>
			<?php
			if(isset($_GET['insert_product'])){
				include("insert_product.php");
			}
			if(isset($_GET['view_products'])){
				include("view_products.php");
			}
			if(isset($_GET['edit_pro'])){
				include("edit_pro.php");
			}
			if(isset($_GET['insert_warehouse'])){
				include("insert_warehouse.php");
			}
			if(isset($_GET['view_warehouse'])){
				include("view_warehouse.php");
			}
			if(isset($_GET['edit_warehouse'])){
				include("edit_warehouse.php");
			}
			if(isset($_GET['finance'])){
				include("finance.php");
			}
			/*if(isset($_GET['view_orders'])){
				include("view_orders.php");
			}
			if(isset($_GET['view_payments'])){
				include("view_payments.php");
			} 
			/*if(isset($_GET['insert_cat'])){
				include("insert_cat.php");
			}
			if(isset($_GET['view_cats'])){
				include("view_cats.php");
			}
			if(isset($_GET['edit_cat'])){
				include("edit_cat.php");
			} */
			?>
			
		</div>
	</div>

</body>

</html>
<?php } ?>
<?php
include("includes/db.php");
	if(isset($_GET['confirm_order'])){
		$get_id = $_GET['confirm_order'];
		$status = 'Completed';
		$update_order = "update orders set status='$status' where order_id='$get_id'";
		$run_update = mysqli_query($con,$update_order);
		if($run_update){
			echo "<script>alert('Užsakymas buvo atnaujintas')</script>";
			echo "<script>window.open('index.php?view_orders','_self')</script>";
		}
	}
?>