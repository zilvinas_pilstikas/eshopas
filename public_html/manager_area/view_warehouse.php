﻿<?php
session_start();
if(($_SESSION['level'] != "vadybininkas")){
	echo "<script>window.open('login.php?not_manager=Jūs ne vadybininkas!','_self')</script>";
}
else{
?>
<table width="795" align="center" bgcolor="pink">
	<tr align="center">
		<td colspan="6"><h2>Peržiūrėti visus sandėlius</h2></td>
	</tr>
	<tr align="center" bgcolor="orange">
		<th>S.N</th>
		<th>Miestas</th>
		<th>Adresas</th>
		<th>Talpa</th>
		<th>Kiekis</th>
		<th>Būsena</th>
		<th>Telefonas</th>
		<th>Redaguoti</th>
	</tr>
	<?php
		include("includes/db.php");
		$get_warehouse = "select * from sandelis";
		$run_warehouse = mysqli_query($con,$get_warehouse);
		$i = 0;
		while($row_warehouse=mysqli_fetch_array($run_warehouse)){
			$warehouse_id = $row_warehouse['sandelio_id'];
			$warehouse_address = $row_warehouse['adresas'];
			$warehouse_city = $row_warehouse['miestas'];
			$warehouse_limit = $row_warehouse['talpa'];
			$warehouse_qty = $row_warehouse['kiekis'];
			$warehouse_state = $row_warehouse['busena'];
			$warehouse_mobile = $row_warehouse['telefonas'];
			$i++;
	?>
	<tr align="center">
		<td><?php echo $i;?></td>
		<td><?php echo $warehouse_city;?></td>
		<td><?php echo $warehouse_address;?></td>
		<td><?php echo $warehouse_limit;?></td>
		<td><?php echo $warehouse_qty;?></td>
		<td><?php echo $warehouse_state;?></td>
		<td><?php echo $warehouse_mobile;?></td>
		<td><a href="index.php?edit_warehouse=<?php echo $warehouse_id;?>">Redaguoti</a></td>
	</tr>
		<?php } ?>
</table>
<?php } ?>