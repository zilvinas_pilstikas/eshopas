﻿<?php
session_start();
if(($_SESSION['level'] != "vadybininkas")){
	echo "<script>window.open('login.php?not_manager=Jūs ne vadybininkas!','_self')</script>";
}
else{
?>
<table width="795" align="center" bgcolor="pink">
	<tr align="center">
		<td colspan="6"><h2>Peržiūrėti finansų ataskaitą</h2></td>
	</tr>
	<tr align="center" bgcolor="orange">
		<th>S.N</th>
		<th>Laikotarpis nuo</th>
		<th>Laikotarpis iki</th>
		<th>Pajamos</th>
		<th>Išlaidos</th>
		<th>Pelnas</th>
	</tr>
	<?php
		include("includes/db.php");
		
		$get_finance = "select * from finansu_ataskaitos";
		$run_finance = mysqli_query($con,$get_finance);
		$i = 0;
		while($row_finance=mysqli_fetch_array($run_finance)){
			$date_from = $row_finance['laikotarpis_nuo'];
			$date_until= $row_finance['laikotarpis_iki'];
			$income = $row_finance['pajamos'];
			$outgoings = $row_finance['islaidos'];
			$gain = $row_finance['pelnas'];
			$i++;
	?>
	<tr align="center">
		<td><?php echo $i;?></td>
		<td><?php echo $date_from;?></td>
		<td><?php echo $date_until; ?></td>
		<td><?php echo $income;?></td>
		<td><?php echo $outgoings;?></td>
		<td><?php echo $gain;?></td>
		
	</tr>
		<?php } ?>
</table>
<?php } ?>