﻿<?php
if(($_SESSION['level'] != "vadybininkas")){
	echo "<script>window.open('login.php?not_manager=Jūs ne vadybininkas!','_self')</script>";
}
else{
?>
<!DOCTYPE>

<?php
include("includes/db.php");
if(isset($_GET['edit_pro'])){
	$get_id = $_GET['edit_pro'];
	$get_pro = "select * from prekes where bruksninis_kodas='$get_id'";
		$run_pro = mysqli_query($con,$get_pro);
		$i = 0;
		$row_pro=mysqli_fetch_array($run_pro);
			$pro_id = $row_pro['bruksninis_kodas'];
			$pro_warehouse_id = $row_pro['FK_sandelio_id'];
			$pro_title = $row_pro['pavadinimas'];
			$pro_brand = $row_pro['gamintojas'];
			$pro_image = $row_pro['nuotrauka'];
			$pro_cat = $row_pro['kategorija'];
			$pro_desc = $row_pro['aprasymas'];
			$pro_keywords = $row_pro['raktazodziai'];
			$pro_price = $row_pro['kaina'];
			$pro_qty = $row_pro['kiekis'];
			$pro_sold = $row_pro['nupirkta'];
}
?>


<html>
	<head>
		<title>Produkto redagavimas</title>
		
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script>tinymce.init({ selector:'textarea' });</script>
	
	</head>
	
<body bgcolor="skyblue">
	<form action="" method="post" enctype="multipart/form-data">
		<table align="center" width="795" border="2" bgcolor="orange">
			<tr align="center">
				<td colspan="7"><h2>Redaguoti ir atnaujinti produktą</h2></td>
			</tr>
			
			<tr>
				<td align="right"><b>Brūkšninis kodas:</b></td>
				<td><input type="text" name="product_code" size ="60" value="<?php echo $pro_id;?>" disabled /></td>
			</tr>
			
			<tr>
				<td align="right"><b>Pavadinimas:</b></td>
				<td><input type="text" name="product_title" size ="60" value="<?php echo $pro_title;?>" required /></td>
			</tr>
			
			<tr>
				<td align="right"><b>Gamintojas:</b></td>
				<td><input type="text" name="product_brand" size ="60" value="<?php echo $pro_brand;?>" required /></td>
			</tr>
			
			<tr>
				<td align="right"><b>Kategorija:</b></td>
				<td><input type="text" name="product_cat" size ="60" value="<?php echo $pro_cat;?>" required /></td>
			</tr>
			
			
			<tr>
				<td align="right"><b>Nuotrauka:</b></td>
				<td><input type="file" name="product_image" /><img src="product_images/<?php echo $pro_image; ?>" width="60" height="60" required /></td>
			</tr>
			
			<tr>
				<td align="right"><b>Kaina:</b></td>
				<td><input type="text" name="product_price" value="<?php echo $pro_price;?>" required /></td>
			</tr>
			
			<tr>
				<td align="right"><b>Kiekis:</b></td>
				<td><input type="text" name="product_qty" value="<?php echo $pro_qty;?>" required /></td>
			</tr>
			
			<tr>
				<td align="right"><b>Nupirkta:</b></td>
				<td><input type="text" name="product_sold" value="<?php echo $pro_sold;?>" disabled /></td>
			</tr>
			
			<tr>
				<td align="right"><b>Produkto Aprašymas:</b></td>
				<td><textarea id="TextArea" name="product_desc" cols="20" rows="10"><?php echo $pro_desc;?></textarea></td>
			</tr>
			
			<tr>
				<td align="right"><b>Produkto raktažodžiai:</b></td>
				<td><input type="text" name="product_keywords" size="50" value="<?php echo $pro_keywords;?>" required /></td>
			</tr>
			
			<tr align="center">
				<td colspan="7"><input type="submit" name="update_product" value="Atnaujinti Produktą" /></td>
			</tr>
			
		</table>
	</form>

</body>
</html>

<?php
	if(isset($_POST['update_product'])){
		//Gaunami tekstiniai duomenys is laukuj
		$product_title = $_POST['product_title'];
		$product_cat = $_POST['product_cat'];
		$product_brand = $_POST['product_brand'];
		$product_price = $_POST['product_price'];
		$product_qty = $_POST['product_qty'];
		$product_desc = $_POST['product_desc'];
		$product_keywords = $_POST['product_keywords'];
		
		$select_warehouse = "select * from sandelis where sandelio_id='$pro_warehouse_id'";
		$run_warehouse = mysqli_query($con,$select_warehouse);
		$row_warehouse = mysqli_fetch_array($run_warehouse);
		$warehouse_limit = $row_warehouse['talpa'];
		$warehouse_qty = $row_warehouse['kiekis'];
		$warehouse_state = $row_warehouse['busena'];
		
		$warehouse_new_qty = $warehouse_qty - $pro_qty + $product_qty;
		
		if ($warehouse_new_qty > $warehouse_limit){
			echo "<script>alert('Produktas buvo nesėkmingai atnaujintas! Į sandėlį netelpa tiek prekių!')</script>";
			echo "<script>window.open('index.php?view_products','_self')</script>";
		}
		
		//Gaunamas paveiksliukas is lauko
		$product_image = $_FILES['product_image']['name'];
		$product_image_tmp = $_FILES['product_image']['tmp_name'];
		move_uploaded_file($product_image_tmp,"product_images/$product_image");
		$update_product = "update prekes set pavadinimas='$product_title', kategorija='$product_cat', gamintojas='$product_brand', kaina='$product_price',
							kiekis='$product_qty',aprasymas='$product_desc', raktazodziai='$product_keywords', nuotrauka='$product_image' where bruksninis_kodas='$pro_id'";
		$run_product = mysqli_query($con,$update_product);
		if($run_product){
			if ($warehouse_new_qty == $warehouse_limit){
				$warehouse_state = "Pilnas";
			} else if ($warehouse_new_qty == 0){
				$warehouse_state = "Tuscias";
			} else{
				$warehouse_state = "Aktyvus";
			}
			$update_warehouse = "update sandelis set kiekis='$warehouse_new_qty', busena='$warehouse_state' where sandelio_id='$pro_warehouse_id'";
			$run_warehouse_update = mysqli_query($con,$update_warehouse);
			
			
			echo "<script>alert('Produktas buvo sėkmingai atnaujintas')</script>";
			echo "<script>window.open('index.php?view_products','_self')</script>";
		}
		else{
			echo "<script>alert('Produktas buvo nesėkmingai atnaujintas')</script>";
		}
	}
?>
<?php } ?>