﻿<?php
session_start();
if(($_SESSION['level'] != "manager")){
	echo "<script>window.open('login.php?not_manager=Jūs ne vadybininkas!','_self')</script>";
}
else{
?>
<table width="795" align="center" bgcolor="pink">
	<tr align="center">
		<td colspan="6"><h2>Peržiūrėti visus užsakymus</h2></td>
	</tr>
	<tr align="center" bgcolor="orange">
		<th>S.N</th>
		<th>Kliento e-paštas</th>
		<th>Pavadinimas</th>
		<th>Kiekis</th>
		<th>Faktūros Nr.</th>
		<th>Užsakymo data</th>
		<th>Statusas</th>
	</tr>
	<?php
		include("includes/db.php");
		
		$get_order = "select * from orders";
		$run_order = mysqli_query($con,$get_order);
		$i = 0;
		while($row_order=mysqli_fetch_array($run_order)){
			$order_id = $row_order['order_id'];
			$qty = $row_order['qty'];
			$pro_id = $row_order['p_id'];
			$c_id = $row_order['c_id'];
			$invoice_no = $row_order['invoice_no'];
			$order_date = $row_order['order_date'];
			$order_status = $row_order['status'];
			$i++;
			$get_pro = "select * from products where product_id='$pro_id'";
			$run_pro = mysqli_query($con,$get_pro);
			$row_pro = mysqli_fetch_array($run_pro);
			$pro_image = $row_pro['product_image'];
			$pro_title = $row_pro['product_title'];
			$get_c = "select * from customers where customer_id='$c_id'";
			$run_c = mysqli_query($con,$get_c);
			$row_c = mysqli_fetch_array($run_c);
			$c_email = $row_c['customer_email'];
	?>
	<tr align="center">
		<td><?php echo $i;?></td>
		<td><?php echo $c_email;?></td>
		<td>
		<?php echo $pro_title;?><br>
		<img src="../admin_area/product_images/<?php echo $pro_image;?>" width="50" height="50" />
		</td>
		<td><?php echo $qty; ?></td>
		<td><?php echo $invoice_no;?></td>
		<td><?php echo $order_date;?></td>
		<?php if ($order_status == "Completed"){ ?>
			<td><?php echo $order_status; ?></td> <?php
		}
		else{ ?>
			<td><a href="index.php?confirm_order=<?php echo $order_id; ?>">Užbaigti užsakymą</a></td> <?php
		} ?>
		
	</tr>
		<?php } ?>
</table>
<?php } ?>
