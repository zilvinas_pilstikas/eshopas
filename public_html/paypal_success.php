﻿<?php
session_start();
?>

<html>
	<head>
			<title>Mokėjimas atliktas sėkmingai!</title>
	</head>
<body>
<?php
include("includes/db.php");
include("functions/functions.php");


		//this is about the customer
		$user = $_SESSION['customer_email'];
		$get_c = "select * from vartotojai where el_pastas='$user'";
		$run_c = mysqli_query($con,$get_c);
		$row_c = mysqli_fetch_array($run_c);
		$c_id = $row_c['id'];
		$c_email = $row_c['el_pastas'];
		$c_name = $row_c['vardas'];
			
	//this is all for product details
	global $con;
	
	$test = 0;
	
	$get_c = "select * from vartotojai where el_pastas='$c_email'";		//paimamas vartotojas
	$run_c = mysqli_query($con,$get_c);
	$row_c = mysqli_fetch_array($run_c);
	$c_id = $row_c['id'];						//paimamas vartotojo id
	
	$get_cart = "select * from prekiu_krepseliai where vartotojo_id='$c_id'";
	$run_cart = mysqli_query($con,$get_cart);
	$cart_row = mysqli_fetch_array($run_cart);
	$cart_id = $cart_row['krepselio_id'];
	$cart_total = $cart_row['prekiu_kaina'];
	
	$get_prod = "select * from kliento_krepselio_prekes where krepselio_id='$cart_id'";			//paimamas krepselis
	$run_prod = mysqli_query($con,$get_prod);
	while($prod_row = mysqli_fetch_array($run_prod)){
		$pro_price = $prod_row['kaina'];
		$pro_qty = $prod_row['kiekis'];
		$pro_bar_code = $prod_row['prekes_bruksninis_kodas'];
									
		$get_title = "select * from prekes where bruksninis_kodas='$pro_bar_code'";				//paimama preke
		$run_title = mysqli_query($con,$get_title);
		$title_row = mysqli_fetch_array($run_title);
		$pro_title = $title_row['pavadinimas'];
		$pro_sold = $title_row['nupirkta'];
		$pro_left_qty = $title_row['kiekis'];
		$pro_warehouse_id = $title_row['FK_sandelio_id'];
		//$pro_image = $title_row['nuotrauka'];
		
		$get_warehouse = "select * from sandelis where sandelio_id='$pro_warehouse_id'";
		$run_warehouse = mysqli_query($con,$get_warehouse);
		$row_warehouse = mysqli_fetch_array($run_warehouse);
		$warehouse_qty = $row_warehouse['kiekis'];
		$warehouse_limit = $row_warehouse['talpa'];
		$warehouse_state = $row_warehouse['busena'];
		
		$mokejimo_budas = "Internetu";
		$pristatymo_budas = "Į namus";
		$pristatymo_kaina = 0;
		$administravimo_mokestis = 0;
		$sukaupti_pinigai = 0;
		$uzsakymo_busena = "Pateiktas";
		
		
		//payment details from paypal
		$amount = $_GET['amt'];
		$currency = $_GET['cc'];
		$trx_id = $_GET['tx'];
		$invoice = mt_rand();
		
		$warehouse_new_qty = $warehouse_qty - $pro_qty;
		$pro_new_left_qty = $pro_left_qty - $pro_qty;
		
		if ($pro_new_left_qty < 0){
			echo "<h2>Sveiki, Mokėjimas buvo nesėkmingas! Sandėlyje nera pakankamai prekių!</h2><br>";
			echo "<a href='http://zilpil.stud.if.ktu.lt'>Grįžti į parduotuvę</a>";
			exit();
		}
		
		//inserting payment to table
		if($test == 0){
			$insert_payment = "insert into kliento_uzsakymai (vartotojo_id,mokejimo_budas,pristatymo_budas,pristatymo_kaina,administravimo_mokestis,prekiu_kaina,
							sukaupti_pinigai,galutine_suma,transakcijos_numeris,fakturos_numeris,uzsakymo_busena,pateikimo_data,fk_VARTOTOJASid) 
					values('$c_id','$mokejimo_budas','$pristatymo_budas','$pristatymo_kaina','$administravimo_mokestis','$cart_total',
							'$sukaupti_pinigai','$amount','$trx_id','$invoice','$uzsakymo_busena',NOW(),'$c_id')";
			$run_payments = mysqli_query($con,$insert_payment);
			
			$select_finance = "select * from finansu_ataskaitos where laikotarpis_nuo < NOW() AND laikotarpis_iki >= NOW()";
			$run_finance = mysqli_query($con,$select_finance);
			$num_rows = mysqli_num_rows($run_finance);
			if ($num_rows > 0){
				$row_finance = mysqli_fetch_array($run_finance);
				$income = $row_finance['pajamos'];
				$outgoings = $row_finance['islaidos'];
				$gain = $row_finance['pelnas'];
				$finance_id = $row_finance['finansu_ataskaitos_id'];
				
				$income = $income + $cart_total;
				$gain = $income - $outgoings;
				
				$update_finance = "update finansu_ataskaitos set pajamos='$income', pelnas='$gain' where finansu_ataskaitos_id='$finance_id'";
				$run_update_finance = mysqli_query($con,$update_finance);
			} else{
				
				$until = date('Y-m-d H:i:s', strtotime('+1 day', time()));
				
				$income = $cart_total;
				$outgoings = 0;
				$gain = $income - $outgoings;
				$insert_finance = "insert into finansu_ataskaitos (laikotarpis_nuo, laikotarpis_iki,pajamos, islaidos, pelnas) 
									values (NOW(),'$until','$income','$outgoings','$gain')";
				$run_insert_finance = mysqli_query($con,$insert_finance);
			}			 
			$test++;
		}
	
		$select_id = "select max(uzsakymo_id) as highest_id from kliento_uzsakymai where vartotojo_id='$c_id'";
		$run_select = mysqli_query($con,$select_id);
		$row_select = mysqli_fetch_array($run_select);
		$get_id = $row_select['highest_id'];
		
		//inserting orders to table
		$insert_order = "insert into kliento_uzsakymo_prekes (uzsakymo_id,prekes_bruksninis_kodas,kiekis,kaina,fk_PREKĖbruksninis_kodas,fk_KLIENTO_UZSAKYMASuzsakymo_id) 
						values('$get_id','$pro_bar_code','$pro_qty','$pro_price','$pro_bar_code','$get_id')";
		$run_order = mysqli_query($con,$insert_order);
		
		
		$pro_sold = $pro_sold + $pro_qty;
		$update_pro = "update prekes set nupirkta='$pro_sold', kiekis='$pro_new_left_qty' where bruksninis_kodas='$pro_bar_code'";
		$run_update = mysqli_query($con,$update_pro);
		
		if ($warehouse_new_qty == 0){
			$warehouse_state = "Tuscias";
		} else if ($warehouse_new_qty == $warehouse_limit){
			$warehouse_state = "Pilnas";
		} else{
			$warehouse_state = "Aktyvus";
		}
		
		$update_warehouse = "update sandelis set kiekis='$warehouse_new_qty', busena='$warehouse_state' where sandelio_id='$pro_warehouse_id'";
		$run_warehouse_update = mysqli_query($con,$update_warehouse);
		
	}
	
	//removing the products from cart
	$empty_cart = "delete from prekiu_krepseliai where vartotojo_id='$c_id'";
	$run_cart = mysqli_query($con,$empty_cart);
	$empty_cart_items = "delete from kliento_krepselio_prekes where krepselio_id='$cart_id'";
	$run_cart_items = mysqli_query($con,$empty_cart_items);
			
	if($cart_total == $amount){
		echo "<h2>Sveiki: " . $_SESSION['customer_email']. "<br>" . "Jūsų mokėjimas buvo sėkmingas!</h2>";
		echo "<a href='http://zilpil.stud.if.ktu.lt/customer/my_account.php'>Eiti į savo paskyrą</a>";
	}
	else{
		echo "<h2>Sveiki, Mokėjimas buvo nesėkmingas!</h2><br>";
		echo "<a href='http://zilpil.stud.if.ktu.lt'>Grįžti į parduotuvę</a>";
	}
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Cotent-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: <zilvinas.pilstikas@ktu.edu>' . "\r\n";
	$subject = "Užsakymo detalės";
	$message = "<html>
	<p>
	
	Sveiki, <b style='color:blue;'>$c_name</b> Jūs užsisakėte produktų iš mūsų svetainės. Žemiau rasite savo užsakymo detales.</p>
	
		<table width='600' align='center' bgolor='#FFCC99' border='2'>
			<tr align='center'><td colspan='6'><h2>Jūsų užsakymo detalės</h2></td></tr>
			
			<tr align='center'>
				<th><b>S.N</b></th>
				<th><b>Produkto pavadinimas</b></th>
				<th><b>Kiekis</b></th>
				<th><b>Sumokėta suma</b></th>
				<th><b>Faktūros Nr.</b></th>
			</tr>
			
			<tr align='center'>
				<td>1</td>
				<td>pro_name</td>
				<td>qty</td>
				<td>total</td>
				<td>invoice</td>
			</tr>
		</table>
		
		<h3> Prašome apsilankyte savo paskyroje ir peržiūrėti savo užsakymą</h3>
		<h2> <a href='http://zilpil.stud.if.ktu.lt'>Spausti čia</a>, kad prisijungti prie savo paskyros</h2>
		<h3> Ačiū už Jūsų užsakymą</h3>
	</html>
	";
	mail($c_email,$subject,$message,$headers);
			
?>
</body>
</html>