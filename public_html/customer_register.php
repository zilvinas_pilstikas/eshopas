<!DOCTYPE>
<?php
session_start();
include("functions/functions.php");
include("includes/db.php");

?>
<html>
	<head>
	<meta charset="utf-8">
		<title>Shopshopas elektroninė parduotuvė</title>
			
	<link rel="stylesheet" href="styles/style.css" media="all" />
	</head

	
	
<body>

	<!--Pagrindinis kontaineris prasideda cia -->
	<div class="main_wrapper">
	
		<!--Headeris prasideda cia -->
		<div class="header_wrapper">
			
			<a href="index.php"><img id="logo" src="images/logo.gif" /> </a>
			<img id="banner" src="images/ad_banner.gif" />
		</div>
		<!--Headeris pasibaigia cia -->
		
		<!--Meniu juosta prasideda cia -->
		<div class="menubar">
			
			<ul id="menu">
				<li><a href="index.php">Namai</a></li>
				<li><a href="all_products.php">Visi produktai</a></li>
				<?php
				if(!isset($_SESSION['customer_email'])){ ?>
					<li><a href="checkout.php">Mano paskyra</a></li> <?php } else { ?>
					<li><a href="customer/my_account.php">Mano paskyra</a></li>
				<?php } ?>
				
				<?php
				if(!isset($_SESSION['customer_email'])){ ?>
					<li><a href="customer_register.php">Užsiregistruoti</a></li>
				<?php } ?>
				<li><a href="cart.php">Krepšelis</a></li>
				<!--<li><a href="#">Kontaktai</a></li> -->
			</ul>
			
			<div id="form">
				<form method="get" action="results.php" enctype="multipart/form-data">
					<input type="text" name="user_query" placeholder="Ieškoti produkto"/>
					<input type="submit" name="search" value="Ieškoti" />
				</form>
			
			</div>
			
		</div>
		<!--Meniu juosta baigiasi cia -->
	
		<!--turinys prasideda cia -->
		<div class="content_wrapper">
		
			<div id="sidebar">
				<div id="sidebar_title">Kategorijos</div>
				
				<ul id="cats">
					<?php getCats(); ?>
				</ul>
			
			
				<div id="sidebar_title">Rūšis</div>
				
				
				<ul id="cats">
					<?php getBrands(); ?>
				</ul>
				
			</div>
	
			<div id="content_area">
			<?php cart(); ?>
				<div id="shopping_cart">
					<span style="float:right; font-size:15px; padding:5px; line-height:40px;">
						<?php 
							if (isset($_SESSION['customer_email'])){
								echo "<b> Sveiki: </b>" . $_SESSION['customer_email'] . "<b style='color:yellow;'> Jūsų</b>";
							}
							else{
								echo "<b>Sveiki svečias:<b/b>";
							}
							$eur = " \xE2\x82\xAc ";
							
						?>
						<b style="color:yellow">Pirkinių krepšelis - </b> Iš viso produktų: <?php total_items(); ?> Bendra suma: <?php echo total_price() . $eur; ?><a href="index.php" style="color:yellow">Grįžti į pradžią</a>
						<?php
							if(!isset($_SESSION['customer_email'])){
								echo "<a href='checkout.php' style='color:orange'>Prisijungti</a>";
							}
							else {
								echo "<a href='logout.php' style='color:orange'>Atsijungti</a>";
							}
						?>
					</span>
				</div>
				
				<form action="customer_register.php" method="post" enctype="multipart/form-data">
					<table align="center" width="550">
						<tr align="center">
							<td colspan="6"><h2>Susikurti paskyrą<h2></td>
						</tr>
						
						<tr>
							<td align="right">Ar esate juridinis asmuo?</td>
							<td><input type="checkbox" name="certificate" value="juridinis" /></td>
						</tr>
						
						<tr align="right">
							<td colspan="6"><input type="submit" name="submit_certificate" value="Pasirinkti"/></td>
						</tr>
						
						<?php
						if(isset($_POST['submit_certificate'])){
							$_SESSION['cert'] = $_POST['certificate'];
						?>
						
						<tr>
							<td align="right">Vardas:</td>
							<td><input type="text" name="c_name" required /></td>
						</tr>
						
						<tr>
							<td align="right">Pavardė:</td>
							<td><input type="text" name="c_surname" required /></td>
						</tr>
						
						<tr>
							<td align="right">Gimimo data:</td>
							<td><input type="date" name="c_birth" required /></td>
						</tr>
						
						<tr>
							<td align="right">E-paštas:</td>
							<td><input type="text" name="c_email" required /></td>
						</tr>
						
						<tr>
							<td align="right">Slaptažodis:</td>
							<td><input type="password" name="c_pass" required /></td>
						</tr>
						
						<tr>
							<td align="right">Lytis:</td>
							<td>
								<select name="c_gender" required >
								<option value="" disabled selected>Pasirinkti lytį</option>
									<option>Vyras</option>
									<option>Moteris</option>
									<option>Kitas</option>
								</select>
							</td>
						</tr>
						
						<tr>
							<td align="right">Miestas:</td>
							<td>
								<select name="c_city" required >
									<option value="" disabled selected>Pasirinkti miestą</option>
									<option>Vilnius</option>
									<option>Kaunas</option>
									<option>Klaipeda</option>
									<option>Panevėžys</option>
									<option>Šiauliai</option>
									<option>Alytus</option>
									<option>Kėdainiai</option>
									<option>Kitas</option>
								</select>
							</td>
						</tr>
						
						<tr>
							<td align="right">Adresas:</td>
							<td><input type="text" name="c_address" required /></td>
						</tr>
						
						<tr>
							<td align="right">Telefonas:</td>
							<td><input type="text" name="c_mobile" required /></td>
						</tr>
						
						<tr>
							<td align="right">Ar norite gauti naujienas el-paštu?</td>
							<td><input type="checkbox" name="c_news_email" value=1/></td>
						</tr>
						
						<tr>
							<td align="right">Ar norite gauti naujienas telefonu?</td>
							<td><input type="checkbox" name="c_news_mobile" value=1/></td>
						</tr>
						
						<?php if($_SESSION['cert'] == "juridinis") { ?>
						
						
						<tr>
							<td align="right">Įmonės pavadinimas:</td>
							<td><input type="text" name="company_name" required /></td>
						</tr>
						
						<tr>
							<td align="right">Įmonės kodas:</td>
							<td><input type="text" name="company_code" required /></td>
						</tr>
						
						<tr>
							<td align="right">Miestas:</td>
							<td><input type="text" name="company_city" required /></td>
						</tr>
						
						<tr>
							<td align="right">Adresas:</td>
							<td><input type="text" name="company_address" required /></td>
						</tr>
						
						<tr>
							<td align="right">Įmonės PVM kodas:</td>
							<td><input type="text" name="company_pvm_code" required /></td>
						</tr>
						
						<?php } ?>
						
						<tr align="right">
							<td colspan="6"><input type="submit" name="register" value="Sukurti paskyrą"/></td>
						</tr>
						
						<?php } ?>
						
					</table>
				</form>
			</div>
		</div>
		<!--turinys baigiasi cia -->
	
	
		<div id="footer">
			<h2 style="text-align:center; padding-top:30px;">&copy; 2016 GO TM elektroninė parduotuvė</h2>
		</div>
	
	
	
	</div>
	<!--Pagrindinis kontaineris pasibaigia cia -->

	
</body>
<html>

<?php
	if(isset($_POST['register'])){
		$c_name = $_POST['c_name'];
		$c_surname = $_POST['c_surname'];
		$c_birth = $_POST['c_birth'];
		$c_email = $_POST['c_email'];
		$c_pass = $_POST['c_pass'];
		$c_gender = $_POST['c_gender'];
		$c_city = $_POST['c_city'];
		$c_address = $_POST['c_address'];
		$c_mobile = $_POST['c_mobile'];
		$c_news_email = $_POST['c_news_email'];
		$c_news_mobile = $_POST['c_news_mobile'];
		$c_group = "klientas";
		$c_bonus_money = 0;
		$c_time_zone = 0;
		$cert = $_SESSION['cert'];
		if ($cert == "juridinis"){
			
		} else{
			$cert = "fizinis";
		}
		
		$insert_c = "insert into vartotojai (el_pastas,slaptazodis,grupe,vardas,pavarde,gimimo_data,lytis,telefonas,miestas,adresas, sertifikato_tipas,naujienos_el_pastu,naujienos_telefonu,sukurimo_data,atnaujinimo_data,sukaupti_pinigai,laiko_zyma)
											values ('$c_email','$c_pass','$c_group','$c_name','$c_surname','$c_birth','$c_gender','$c_mobile','$c_city','$c_address','$cert','$c_news_email','$c_news_mobile',NOW(),NOW(),'$c_bonus_money','$c_time_zone')";
		$run_c = mysqli_query($con,$insert_c);
		
		$sel_c = "select * from vartotojai where slaptazodis='$c_pass' AND el_pastas='$c_email'";
		$run_cus = mysqli_query($con,$sel_c);
		$customer_row = mysqli_fetch_array($run_cus);
		$vartotojo_id = $customer_row['id'];
		
		if ($_SESSION['cert'] == "juridinis"){
			$company_name = $_POST['company_name'];
			$company_code = $_POST['company_code'];
			$company_city = $_POST['company_city'];
			$company_address = $_POST['company_address'];
			$company_pvm_code = $_POST['company_pvm_code'];
			
			$insert_company = "insert into juridinio_asmens_informacijos (imonės_pavadinimas,imonės_kodas,miestas,adresas,imonės_PVM_kodas,fk_VARTOTOJASid)
								values ('$company_name','$company_code','$company_city','$company_address','$company_pvm_code','$vartotojo_id')";
			
			$run_company = mysqli_query($con,$insert_company);
		}
		
		$sel_cart = "select * from prekiu_krepseliai  where vartotojo_id='$vartotojo_id'";
		$run_cart = mysqli_query($con,$sel_cart);
		$check_cart = mysqli_num_rows($run_cart);
		if($check_cart==0){
			$_SESSION['customer_email']=$c_email;
			echo "<script><alert('Paskyra buvo sėkmingai sukurta!')</script>";
			echo "<script>window.open('customer/my_account.php','_self')</script>";
		}
		else {
			$_SESSION['customer_email']=$c_email;
			echo "<script><alert('Paskyra buvo sėkmingai sukurta!')</script>";
			echo "<script>window.open('checkout.php','_self')</script>";
		}
	}
?>