﻿<?php
session_start();
if(($_SESSION['level'] != "buhalteris")){
	echo "<script>window.open('login.php?not_admin=Jūs neturite teisių!','_self')</script>";
}
else{
	$dbc = mysqli_connect('stud.if.ktu.lt', 'zilpil', 'ush5ei1aeph1af9E', 'zilpil');
	if(!$dbc ){
		die('Negaliu prisijungti: '.mysqli_error($dbc));
	}
	if (mysqli_connect_errno()) {
		die('Connect failed: '.mysqli_connect_errno().' : '.
		mysqli_connect_error());
	}
	mysqli_set_charset($dbc, 'utf8');
	$ivykioID = -1;
	$vartotojoLygis = "";
	setcookie("vartotojoLygis", "", time()-3600);
	setcookie("pasirinktasĮvykis", "", time()-3600);
	$i = 0;
	
	
	if(isset($_COOKIE['vartotojoLygis'])) {
		$vartotojoLygis = $_COOKIE['vartotojoLygis'];
		$ivykioID = $_COOKIE['pasirinktasĮvykis'];
	}
	
	if(isset($_POST["ok"])) {
		if ($ivykioID != -1 && $vartotojoLygis != "") {
			mysqli_query($dbc, "
				UPDATE vartotojai 
				SET grupe = '$vartotojoLygis'
				WHERE id = $ivykioID
			");
			
	//		$q = "DELETE FROM vartotojai WHERE id = '$ivykioID'";
    //        $database->query($q);
			header("Refresh:0");
		}
		else {
		echo ("<SCRIPT LANGUAGE='JavaScript'>
		window.alert('Neatlikote pakeitimų');
		</SCRIPT>");
		}
	}
	
	$irasai = mysqli_query($dbc, "SELECT * FROM vartotojai");
	if(isset($_POST["filtruoti"])) {
		$pasirinktasElPastas = $_POST['elPastas'];
		$irasai = mysqli_query($dbc, "SELECT * FROM vartotojai WHERE el_pastas = '$pasirinktasElPastas'");
	}
	
	$blokuoti_klientai = mysqli_query($dbc, "SELECT * FROM uzblokuoti_klientai");
	$eiluciu_kiekis = mysqli_num_rows($irasai);
	
?>
<html>
<head>
<meta charset="UTF-8">

<script type="text/javascript">

	function pasirinktasID(id) {
		document.cookie="pasirinktasĮvykis="+ id;
	}

	function updateLevel(nr) {
		document.cookie="vartotojoLygis="+ document.querySelectorAll('#lygis')[nr].value;
		<?php 
		$nr = 0;
			while($nr < $eiluciu_kiekis):
		?>
		var numeris = "<?php Print($nr); ?>";
		if (nr != numeris) { 
			document.querySelectorAll('#lygis')[numeris].disabled = true;
		}
		<?php $nr++; ?>
		<?php endwhile; ?>		
	}

</script>	


<style>
	.lentele{
	width:100%; 
	border-collapse:collapse; 
	}
	.lentele th {
		background-color: #4CAF50;
		color: white;
		text-align: center;
	}
	.lentele td{ 
	padding:7px; border:#4e95f4 1px solid;
	text-align: center;
	}
	.lentele tr:nth-child(even){
		background-color: #EAC7FF;
	}
	.lentele tr:hover{  background-color: #ffff99; }
	.zymejimas:hover{  background-color: #ffff99; }
	
	.mygtukas {
	   border-radius: 5px;
		border: 0;
		width: 65px;
		height:20px;
		font-family: Tahoma;
		background: #f4f4f4;
		/* Old browsers */
		background: -moz-linear-gradient(top, #f4f4f4 1%, #ededed 100%);
		/* FF3.6+ */
		background: -webkit-gradient(linear, left top, left bottom, color-stop(1%, #f4f4f4), color-stop(100%, #ededed));
		/* Chrome,Safari4+ */
		background: -webkit-linear-gradient(top, #f4f4f4 1%, #ededed 100%);
		/* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(top, #f4f4f4 1%, #ededed 100%);
		/* Opera 11.10+ */
		background: -ms-linear-gradient(top, #f4f4f4 1%, #ededed 100%);
		/* IE10+ */
		background: linear-gradient(to bottom, #f4f4f4 1%, #ededed 100%);
		/* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f4f4f4', endColorstr='#ededed', GradientType=0);
	}
	
	.mygtukas2 {
	   border-radius: 5px;
		border: 0;
		width: 80px;
		height:25px;
		font-family: Tahoma;
		background: #f4f4f4;
		/* Old browsers */
		background: -moz-linear-gradient(top, #f4f4f4 1%, #ededed 100%);
		/* FF3.6+ */
		background: -webkit-gradient(linear, left top, left bottom, color-stop(1%, #f4f4f4), color-stop(100%, #ededed));
		/* Chrome,Safari4+ */
		background: -webkit-linear-gradient(top, #f4f4f4 1%, #ededed 100%);
		/* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(top, #f4f4f4 1%, #ededed 100%);
		/* Opera 11.10+ */
		background: -ms-linear-gradient(top, #f4f4f4 1%, #ededed 100%);
		/* IE10+ */
		background: linear-gradient(to bottom, #f4f4f4 1%, #ededed 100%);
		/* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f4f4f4', endColorstr='#ededed', GradientType=0);
	}
	
	.datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; border: 1px solid #652299; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; }.datagrid table td, .datagrid table th { padding: 3px 10px; }.datagrid table thead th {background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #652299), color-stop(1, #4D1A75) );background:-moz-linear-gradient( center top, #652299 5%, #4D1A75 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#652299', endColorstr='#4D1A75');background-color:#652299; color:#FFFFFF; font-size: 12px; font-weight: bold; border-left: 1px solid #714399; } .datagrid table thead th:first-child { border: none; }.datagrid table tbody td { color: #000000; border-left: 1px solid #000000;font-size: 14px;font-weight: normal; }.datagrid table tbody .alt td { background: #F4E3FF; color: #4D1A75; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }.datagrid table tfoot td div { border-top: 1px solid #652299;background: #F4E3FF;} .datagrid table tfoot td { padding: 0; font-size: 12px } .datagrid table tfoot td div{ padding: 2px; }.datagrid table tfoot td ul { margin: 0; padding:0; list-style: none; text-align: right; }.datagrid table tfoot  li { display: inline; }.datagrid table tfoot li a { text-decoration: none; display: inline-block;  padding: 2px 8px; margin: 1px;color: #FFFFFF;border: 1px solid #652299;-webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #652299), color-stop(1, #4D1A75) );background:-moz-linear-gradient( center top, #652299 5%, #4D1A75 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#652299', endColorstr='#4D1A75');background-color:#652299; }.datagrid table tfoot ul.active, .datagrid table tfoot ul a:hover { text-decoration: none;border-color: #4D1A75; color: #FFFFFF; background: none; background-color:#652299;}div.dhtmlx_window_active, div.dhx_modal_cover_dv { position: fixed !important; }
</style>
<head>
	<div style="text-align: center;color:black">
		<h1>Registruotų vartotojų sąrašas</h1>
	</div>
	
	<form method='post'>
		<label for="elPastas" class="control-label">El. paštas:</label>
		<input name='elPastas' id="elPastas" type='input' class="form-control input-sm" style="width: 180px">
		<br>
		<input type='submit' name='filtruoti' value='Filtruoti' class="btn btn-default">
	</form>
	
<table class="lentele" >
<thead>
	<tr align="center" bgcolor="orange">
		<th>El. paštas</th>
		<th>Vardas</th>
		<th>Pavardė</th>
		<th>Lygis</th>
		<th>Telefonas</th>
		<th>Veiksmai</th>
	</tr>
	</thead>
	<?php //Imami irasai is lenteles kol yra irasu
		while($irasas = mysqli_fetch_assoc($irasai)): 
		$vartotojo_ID = $irasas['id'];
		$blokuoti = mysqli_query($dbc, "SELECT vartotojo_id FROM uzblokuoti_klientai WHERE vartotojo_id = '$vartotojo_ID'");
		if ($blokuoti->num_rows == 0):
		?>
		<tr>
			<td><?php  echo "<div style='text-align:center'>".$irasas["el_pastas"]."</div>";?></td>
			<td><?php  echo "<div style='text-align:center'>".$irasas["vardas"]."</div>";?></td>
			<td><?php  echo "<div style='text-align:center'>".$irasas["pavarde"]."</div>";?></td>
			
			<form method='post'>	
			<td>
			<select onchange="pasirinktasID(<?php echo $irasas['id'] ?>), updateLevel(<?php echo $i ?>)" id='lygis' style="width: 80px" >
			
			<option <?php if (htmlspecialchars($irasas["grupe"]) == "klientas"): ?>
			selected
			<?php endif; ?>
			value="klientas">Klientas
			</option>
			
			<option <?php if (htmlspecialchars($irasas["grupe"]) == "buhalteris"): ?>
			selected
			<?php endif; ?>
			value="buhalteris">Buhalteris
			</option>

			<option <?php if (htmlspecialchars($irasas["grupe"]) == "vadybininkas"): ?>
			selected
			<?php endif; ?>
			value="vadybininkas">Vadybininkas
			</option>			
			
			</td>
			
			<td><?php  echo "<div style='text-align:center'>".$irasas["telefonas"]."</div>";?></td>
			<td> <input type='submit' name='ok' value='Atnaujinti' class="mygtukas"> 
			<a id='blokuoti' href="block_user.php?id=<?php echo $irasas['id'];?>">Blokuoti</a>
			<a id='trinti' href="delete_user.php?id=<?php echo $irasas['id'];?>">Ištrinti</a>
			</td>
			<?php $i++ ?>
			</form>
			<?php endif; ?>
		</tr>
	<?php endwhile; ?>
	
</table>

	<div style="text-align: center;color:black">
	<br><br>
		<h1>Blokuotų vartotojų sąrašas</h1>
	</div>
	
<table class="lentele" >
<thead>
	<tr align="center" bgcolor="orange">
		<th>Vartotojo ID</th>
		<th>Vartotojo el. paštas</th>
		<th>Užblokavimo data</th>
		<th>Veiksmai</th>
	</tr>
	</thead>
	<?php //Imami irasai is lenteles kol yra irasu
		while($irasas = mysqli_fetch_assoc($blokuoti_klientai)): 
		$blokuoto_vartotojo_ID = $irasas['vartotojo_id'];
		$blokuoto_kliento_inforamcija = mysqli_query($dbc, "SELECT el_pastas FROM vartotojai WHERE id = '$blokuoto_vartotojo_ID'");
		$blokuoto_kliento_pastas = mysqli_fetch_assoc($blokuoto_kliento_inforamcija);
		
		?>
		<tr>
			<td><?php  echo "<div style='text-align:center'>".$irasas["vartotojo_id"]."</div>";?></td>
			<td><?php  echo "<div style='text-align:center'>".$blokuoto_kliento_pastas["el_pastas"]."</div>";?></td>
			<td><?php  echo "<div style='text-align:center'>".$irasas["uzblokavimo_data"]."</div>";?></td>
			<td>
			<a href="unblock_user.php?id=<?php echo $irasas['vartotojo_id'];?>">Atblokuoti</a>
			<a href="delete_user.php?id=<?php echo $irasas['vartotojo_id'];?>">Ištrinti</a>
			</td>
		</tr>
	<?php endwhile; ?>
	
</table>	


</html>
<?php } ?>