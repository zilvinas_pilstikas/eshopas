<?php
include("includes/session.php");
if (!$session->logged_in && $_SESSION['level'] != "buhalteris") {
	echo "<script>window.open('login.php?not_admin=Jūs neturite teisių!','_self')</script>";
}
else {
	$dbc = mysqli_connect('stud.if.ktu.lt', 'zilpil', 'ush5ei1aeph1af9E', 'zilpil');
	if(!$dbc ){
		die('Negaliu prisijungti: '.mysqli_error($dbc));
	}
	if (mysqli_connect_errno()) {
		die('Connect failed: '.mysqli_connect_errno().' : '.
		mysqli_connect_error());
	}
	mysqli_set_charset($dbc, 'utf8');
	
	if (isset($_GET['id'])) {
		$uzsakymo_id = trim($_GET['id']);
		$irasai = mysqli_query($dbc, "SELECT * FROM kliento_uzsakymai WHERE uzsakymo_id = '$uzsakymo_id'");
		$kliento_uzsakymo_prekes =  mysqli_query($dbc, "SELECT * FROM kliento_uzsakymo_prekes WHERE uzsakymo_id = '$uzsakymo_id'");
	}
	if(isset($_POST["grizti"])) {
		echo "<script>window.open('index.php?view_orders','_self')</script>";
	}
?>

<!DOCTYPE>
<html>
	<head>
		<link rel="stylesheet" href="styles/style.css" media="all"/>
		<script type="text/javascript">
			function makeTableScroll() {
				// Constant retrieved from server-side via JSP
				var maxRows = 3;

				var table = document.getElementById('myTable');
				var wrapper = table.parentNode;
				var rowsInTable = table.rows.length;
				var height = 0;
				if (rowsInTable > maxRows) {
					for (var i = 0; i < maxRows; i++) {
						height += table.rows[i].clientHeight;
					}
					wrapper.style.height = height + "px";
				}
			}
		</script>	
		
		<style>
			.lentele{
			width:100%; 
			border-collapse:collapse; 
			}
			.lentele th {
				border:#000000 1px solid;
				background-color: #d9d9d9;
				color: black;
				text-align: center;
				height: 40px;
				width: 160px;
			}
			.lentele td{ 
			padding:7px; border:#000000 1px solid;
			text-align: center;
			height: 40px;
			width: 180px;
			}
			
			.lentele2{
			width:100%; 
			border-collapse:collapse; 
			}
			.lentele2 th {
				border:#000000 1px solid;
				background-color: #d9d9d9;
				color: black;
				text-align: center;
			}
			.lentele2 td{ 
			padding:7px; border:#000000 1px solid;
			text-align: center;
			}
			.lentele2 tr:nth-child(even){
				background-color: #EAC7FF;
			}
			.lentele2 tr:hover{  background-color: #ffff99; }
			
			#vertical-2 thead,#vertical-2 tbody{
                display:inline-block;
            }
			.scrollingTable {  
            overflow-y: auto;
			}
		</style>
		<meta charset="UTF-8">
	</head>
	
	
<body>
	<div class="main_wrapper">
		<div id="header"></div>
		<div id="right">
			<h2 style="text-align:center;">Valdyti turinį</h2>
			<a href="index.php?view_customers">Peržiūrėti klientus</a>
			<a href="index.php?view_orders">Peržiūrėti užsakymus</a>
			<a href="index.php?popular_products">Populiariausios prekės</a>
			<a href="index.php?loyal_customers">Lojalūs klientai</a>
			<a href="logout.php">Atsijungti</a>
		</div>
		<div id="left">
		<h2 style="color:red; text-align:center;"><?php echo @$_GET['logged_in']; ?></h2>
			<?php
			if(isset($_GET['view_customers'])){
				include("view_customers.php");
			}
			if(isset($_GET['view_orders'])){
				include("view_orders.php");
			}
			if(isset($_GET['popular_products'])){
				include("popular_products.php");
			}
			if(isset($_GET['loyal_customers'])){
				include("loyal_customers.php");
			}
			?>
			<br>
			<table id="vertical-2" class="lentele">
				<thead>
				<tr align="center">
					<th colspan="3">Užsakymo id</th>
				</tr>
				<tr align="center">
					<th colspan="3">Kliento vardas</th>
				</tr>
				<tr align="center">
					<th colspan="3">Kliento pavardė</th>
				</tr>
				<tr align="center">
					<th colspan="3">Mokėjimo būdas</th>
				</tr>
				<tr align="center">
					<th colspan="3">Pristatymo būdas</th>
				</tr>				
				<tr align="center">
					<th colspan="3">Pristatymo kaina</th>
				</tr>
				<tr align="center">
					<th colspan="3">Administravimo mokestis</th>
				</tr>
				<tr align="center">
					<th colspan="3">Prekių kaina</th>
				</tr>	
				<tr align="center">
					<th colspan="3">Galutinė kaina</th>
				</tr>
				<tr align="center">
					<th colspan="3">Užsakymo būsena</th>
				</tr>
				<tr align="center">
					<th colspan="3">Pateikimo data</th>
				</tr>	
				<tr align="center">
					<th colspan="3">Vykdymo pradžios data</th>
				</tr>	
				<tr align="center">
					<th colspan="3">Paruošimo atsiimti data</th>
				</tr>	
				<tr align="center">
					<th colspan="3">Įvykdymo data</th>
				</tr>					
				</thead>
				<tbody>
					<?php //Imami irasai is lenteles kol yra irasu
						while($irasas = mysqli_fetch_assoc($irasai)): 
							$kliento_ID = $irasas['vartotojo_id'];
							$kliento_inforamcija = mysqli_query($dbc, "SELECT vardas, pavarde FROM vartotojai WHERE id = '$kliento_ID'");
							$kliento_irasas = mysqli_fetch_assoc($kliento_inforamcija);
					?>
					<tr>
						<td><?php  echo "<div style='text-align:center'>".$irasas["uzsakymo_id"]."</div>";?></td>
					</tr>
					<tr>
						<td><?php  echo "<div style='text-align:center'>".$kliento_irasas["vardas"]."</div>";?></td>
					</tr>	
					<tr>
						<td><?php  echo "<div style='text-align:center'>".$kliento_irasas["pavarde"]."</div>";?></td>
					</tr>		
					<tr>
						<td><?php  echo "<div style='text-align:center'>".$irasas["mokejimo_budas"]."</div>";?></td>
					</tr>
					<tr>
						<td><?php  echo "<div style='text-align:center'>".$irasas["pristatymo_budas"]."</div>";?></td>
					</tr>
					<tr>
						<td><?php  echo "<div style='text-align:center'>".$irasas["pristatymo_kaina"]."</div>";?></td>
					</tr>
					<tr>
						<td><?php  echo "<div style='text-align:center'>".$irasas["administravimo_mokestis"]."</div>";?></td>
					</tr>	
					<tr>
						<td><?php  echo "<div style='text-align:center'>".$irasas["prekiu_kaina"]."</div>";?></td>
					</tr>	
					<tr>
						<td><?php  echo "<div style='text-align:center'>".$irasas["galutine_suma"]."</div>";?></td>
					</tr>
					<tr>
						<td><?php  echo "<div style='text-align:center'>".$irasas["uzsakymo_busena"]."</div>";?></td>
					</tr>
					<tr>
						<td><?php  echo "<div style='text-align:center'>".$irasas["pateikimo_data"]."</div>";?></td>
					</tr>	
					<tr>
						<td><?php  echo "<div style='text-align:center'>".$irasas["vykdymo_pradzios_data"]."</div>";?></td>
					</tr>
					<tr>
						<td><?php  echo "<div style='text-align:center'>".$irasas["paruosimo_atsiimti_data"]."</div>";?></td>
					</tr>
					<tr>
						<td><?php  echo "<div style='text-align:center'>".$irasas["ivykdymo_data"]."</div>";?></td>
					</tr>					
					<?php endwhile; ?>
				 </tbody>
			</table>
			<br>
			
			<body onload="makeTableScroll();">
			<div class="scrollingTable">
			<table class="lentele2" id="myTable">
			<thead>	
				<tr align="center">
					<th>Prekės brūkšninis kodas</th>
					<th>Vieneto kaina</th>
					<th>Kiekis</th>
				</tr>
			</thead>
					<?php //Imami irasai is lenteles kol yra irasu
						while($irasas = mysqli_fetch_assoc($kliento_uzsakymo_prekes)): 
					?>
					<tr>
						<td><?php  echo "<div style='text-align:center'>".$irasas["prekes_bruksninis_kodas"]."</div>";?></td>
						<td><?php  echo "<div style='text-align:center'>".$irasas["kaina"]."</div>";?></td>
						<td><?php  echo "<div style='text-align:center'>".$irasas["kiekis"]."</div>";?></td>
					</tr>	
					<?php endwhile; ?>
			</table>
			</div>
			</body>
			<br>
			<form method='post'>
				<input type='submit' name='grizti' value='Grįžti atgal' class="btn btn-default">
			</form>
		</div>
	</div>

</body>	
	
</html>
<?php } ?>