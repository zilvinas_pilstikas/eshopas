﻿<?php
session_start();
?>
<!DOCTYPE>
<html>
	<head>
		<title>Prisijungimo forma</title>
		<link rel="stylesheet" href="styles/login_style.css" media="all" />
	</head>
<body>
<div class="login">
<h2 style="color:white; text-align:center;"><?php echo @$_GET['logged_out']; ?></h2>
	<h1>Buhalterio Prisijungimas</h1>
    <form method="post" action="login.php">
    	<input type="text" name="email" placeholder="E-paštas" required="required" />
        <input type="password" name="password" placeholder="Slaptažodis" required="required" />
        <button type="submit" class="btn btn-primary btn-block btn-large" name="login">Prisijungti</button>
    </form>
</div>
</body>
</html>
<?php
include("includes/db.php");
	if(isset($_POST['login'])){
		$email = mysqli_real_escape_string($con,$_POST['email']);
		$pass = mysqli_real_escape_string($con,$_POST['password']);
		$sel_user = "select * from vartotojai where el_pastas='$email' AND slaptazodis='$pass'";
		$run_user = mysqli_query($con,$sel_user);
		$check_user = mysqli_num_rows($run_user);
		if($check_user == 0){
			echo "<script>alert('Slaptažodis arba e-paštas neteisingas, bandykite dar kartą!')</script>";
		}
		else{
			$row_user = mysqli_fetch_array($run_user);
			$level = $row_user['grupe'];
			$_SESSION['level']=$level;
			$_SESSION['el_pastas']=$email;
			echo "<script>window.open('index.php?logged_in=Jūs sėkmingai prisijungėte','_self')</script>";
		}
	}
?>