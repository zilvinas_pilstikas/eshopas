﻿<?php
session_start();
if(($_SESSION['level'] != "buhalteris")){
	echo "<script>window.open('login.php?not_admin=Jūs neturite teisių!','_self')</script>";
}
else{
	$dbc = mysqli_connect('stud.if.ktu.lt', 'zilpil', 'ush5ei1aeph1af9E', 'zilpil');
	if(!$dbc ){
		die('Negaliu prisijungti: '.mysqli_error($dbc));
	}
	if (mysqli_connect_errno()) {
		die('Connect failed: '.mysqli_connect_errno().' : '.
		mysqli_connect_error());
	}
	mysqli_set_charset($dbc, 'utf8');
	define('TIMEZONE', 'Europe/Vilnius');
	date_default_timezone_set(TIMEZONE);
	$date = date('Y/m/d H:i:s');
	
	$ivykioID = -1;
	$uzsakymoBusena = "";
	setcookie("uzsakymoBusena", "", time()-3600);
	setcookie("pasirinktasĮvykis", "", time()-3600);
	$i = 0;
	
	if(isset($_COOKIE['uzsakymoBusena'])) {
		$uzsakymoBusena = $_COOKIE['uzsakymoBusena'];
		$ivykioID = $_COOKIE['pasirinktasĮvykis'];
	}
	
	if(isset($_POST["ok"])) {
		if ($ivykioID != -1 && $uzsakymoBusena != "") {
			if ($uzsakymoBusena == "Pateiktas") {
				mysqli_query($dbc, "
					UPDATE kliento_uzsakymai 
					SET uzsakymo_busena = '$uzsakymoBusena',
					pateikimo_data = '$date'
					WHERE uzsakymo_id = $ivykioID
				");
			}
			if ($uzsakymoBusena == "Pradėtas vykdyti") {
				mysqli_query($dbc, "
					UPDATE kliento_uzsakymai 
					SET uzsakymo_busena = '$uzsakymoBusena',
					vykdymo_pradzios_data = '$date'
					WHERE uzsakymo_id = $ivykioID
				");
			}
			if ($uzsakymoBusena == "Paruoštas atsiimti") {
				mysqli_query($dbc, "
					UPDATE kliento_uzsakymai 
					SET uzsakymo_busena = '$uzsakymoBusena',
					paruosimo_atsiimti_data = '$date'
					WHERE uzsakymo_id = $ivykioID
				");
			}
			if ($uzsakymoBusena == "Įvykdytas") {
				mysqli_query($dbc, "
					UPDATE kliento_uzsakymai 
					SET uzsakymo_busena = '$uzsakymoBusena',
					ivykdymo_data = '$date'
					WHERE uzsakymo_id = $ivykioID
				");
			}		
		//	header("Refresh:0");
		}
		else {
		echo ("<SCRIPT LANGUAGE='JavaScript'>
		window.alert('Neatlikote pakeitimų');
		</SCRIPT>");
		}
	}
	$irasai = mysqli_query($dbc, "SELECT * FROM kliento_uzsakymai");
	
	if(isset($_POST["filtruoti"])) {
		$pasirinktasID = $_POST['uzsakymoID'];
		$pasirinktasVardas = $_POST['klientoVardas'];
		$pasirinktaPavarde = $_POST['klientoPavarde'];
		$pasirinktaBusena = $_POST['pasirinktaBusena'];	
		if (isset($pasirinktasID) && $pasirinktasID != "") {
			$irasai = mysqli_query($dbc, "SELECT * FROM kliento_uzsakymai WHERE uzsakymo_id = $pasirinktasID");
		}
		else { 
			if (isset($pasirinktaBusena) && $pasirinktaBusena != "") {
				$irasai = mysqli_query($dbc, "SELECT * FROM kliento_uzsakymai WHERE uzsakymo_busena = '$pasirinktaBusena'");
			}
		}
	}
	
	if(isset($_POST["rodytiVisus"])) {
		$irasai = mysqli_query($dbc, "SELECT * FROM kliento_uzsakymai");
	}	
	
	$i = 0;
	$eiluciu_kiekis = mysqli_num_rows($irasai);
	
?>
<html>
<head>
<meta charset="UTF-8">

<script type="text/javascript">

	function pasirinktasID(id) {
		document.cookie="pasirinktasĮvykis="+ id;
	}

	function atnaujintiBusena(nr) {
		document.cookie="uzsakymoBusena="+ document.querySelectorAll('#busena')[nr].value;
		<?php 
		$nr = 0;
			while($nr < $eiluciu_kiekis):
		?>
		var numeris = "<?php Print($nr); ?>";
		if (nr != numeris) { 
			document.querySelectorAll('#busena')[numeris].disabled = true;
		}
		<?php $nr++; ?>
		<?php endwhile; ?>		
	}
	
	function makeTableScroll() {
            // Constant retrieved from server-side via JSP
            var maxRows = 15;

            var table = document.getElementById('myTable');
            var wrapper = table.parentNode;
            var rowsInTable = table.rows.length;
            var height = 0;
            if (rowsInTable > maxRows) {
                for (var i = 0; i < maxRows; i++) {
                    height += table.rows[i].clientHeight;
                }
                wrapper.style.height = height + "px";
            }
     }

</script>	


<style>
	.lentele{
	width:100%; 
	border-collapse:collapse; 
	}
	.lentele th {
		background-color: #4CAF50;
		color: white;
		text-align: center;
	}
	.lentele td{ 
	padding:7px; border:#4e95f4 1px solid;
	text-align: center;
	}
	.lentele tr:nth-child(even){
		background-color: #EAC7FF;
	}
	.lentele tr:hover{  background-color: #ffff99; }
	.zymejimas:hover{  background-color: #ffff99; }
	
	.mygtukas {
	   border-radius: 5px;
		border: 0;
		width: 65px;
		height:20px;
		font-family: Tahoma;
		background: #f4f4f4;
		/* Old browsers */
		background: -moz-linear-gradient(top, #f4f4f4 1%, #ededed 100%);
		/* FF3.6+ */
		background: -webkit-gradient(linear, left top, left bottom, color-stop(1%, #f4f4f4), color-stop(100%, #ededed));
		/* Chrome,Safari4+ */
		background: -webkit-linear-gradient(top, #f4f4f4 1%, #ededed 100%);
		/* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(top, #f4f4f4 1%, #ededed 100%);
		/* Opera 11.10+ */
		background: -ms-linear-gradient(top, #f4f4f4 1%, #ededed 100%);
		/* IE10+ */
		background: linear-gradient(to bottom, #f4f4f4 1%, #ededed 100%);
		/* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f4f4f4', endColorstr='#ededed', GradientType=0);
	}
	.scrollingTable {  
            overflow-y: auto;
        }
	
	.datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; border: 1px solid #652299; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; }.datagrid table td, .datagrid table th { padding: 3px 10px; }.datagrid table thead th {background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #652299), color-stop(1, #4D1A75) );background:-moz-linear-gradient( center top, #652299 5%, #4D1A75 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#652299', endColorstr='#4D1A75');background-color:#652299; color:#FFFFFF; font-size: 12px; font-weight: bold; border-left: 1px solid #714399; } .datagrid table thead th:first-child { border: none; }.datagrid table tbody td { color: #000000; border-left: 1px solid #000000;font-size: 14px;font-weight: normal; }.datagrid table tbody .alt td { background: #F4E3FF; color: #4D1A75; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }.datagrid table tfoot td div { border-top: 1px solid #652299;background: #F4E3FF;} .datagrid table tfoot td { padding: 0; font-size: 12px } .datagrid table tfoot td div{ padding: 2px; }.datagrid table tfoot td ul { margin: 0; padding:0; list-style: none; text-align: right; }.datagrid table tfoot  li { display: inline; }.datagrid table tfoot li a { text-decoration: none; display: inline-block;  padding: 2px 8px; margin: 1px;color: #FFFFFF;border: 1px solid #652299;-webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #652299), color-stop(1, #4D1A75) );background:-moz-linear-gradient( center top, #652299 5%, #4D1A75 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#652299', endColorstr='#4D1A75');background-color:#652299; }.datagrid table tfoot ul.active, .datagrid table tfoot ul a:hover { text-decoration: none;border-color: #4D1A75; color: #FFFFFF; background: none; background-color:#652299;}div.dhtmlx_window_active, div.dhx_modal_cover_dv { position: fixed !important; }
</style>
<head>
	<div style="text-align: center;color:black">
		<h1>Klientų užsakymai</h1>
	</div>
	<br>
	<form method='post'>
		<label for="uzsakymoID" class="control-label">Užsakymo id:</label>
		<input name='uzsakymoID' id="uzsakymoID" type='input' class="form-control input-sm" style="width: 60px">
		<br><br>
		<label for="klientoVardas" class="control-label">Kliento vardas:</label>
		<input name='klientoVardas' id="klientoVardas" type='input' class="form-control input-sm" style="width: 80px">
		<label for="klientoPavarde" class="control-label">Kliento pavardė:</label>
		<input name='klientoPavarde' id="klientoPavarde" type='input' class="form-control input-sm" style="width: 80px">
		<br><br>
		<label for="pasirinktaBusena" class="control-label">Užsakymo būsena:</label>
		<select name='pasirinktaBusena' id='pasirinktaBusena' style="width: 120px" >
		<option value=""></option>
		<option value="Pateiktas">Pateiktas</option>
		<option value="Pradėtas vykdyti">Pradėtas vykdyti</option>
		<option value="Paruoštas atsiimti">Paruoštas atsiimti</option>
		<option value="Įvykdytas">Įvykdytas</option>
		</select>
		<br><br>
		<input type='submit' name='filtruoti' value='Filtruoti' class="btn btn-default">
		<input type='submit' name='rodytiVisus' value='Rodyti visus' class="btn btn-default">
	</form>
	<br>
<body onload="makeTableScroll();">
 <div class="scrollingTable">	
<table class="lentele" id="myTable">
<thead>
	<tr align="center" bgcolor="orange">
		<th>Užsakymo id</th>
		<th>Kliento vardas</th>
		<th>Kliento pavardė</th>
		<th>Galutinė suma</th>
		<th>Pristatymo būdas</th>
		<th>Užsakymo būsena</th>
		<th>Veiksmai</th>
	</tr>
	</thead>
	<?php //Imami irasai is lenteles kol yra irasu
	$i=0;
		while($irasas = mysqli_fetch_assoc($irasai)): 
			$kliento_ID = $irasas['vartotojo_id'];
			$kliento_inforamcija = mysqli_query($dbc, "SELECT vardas, pavarde FROM vartotojai WHERE id = '$kliento_ID'");
			$kliento_irasas = mysqli_fetch_assoc($kliento_inforamcija);
	?>
	<?php
			if (isset($pasirinktasVardas) && $pasirinktasVardas != "" && isset($pasirinktaPavarde) && $pasirinktaPavarde != "") {
				if ($kliento_irasas["vardas"] == $pasirinktasVardas && $kliento_irasas["pavarde"] == $pasirinktaPavarde) {
				?>
			<tr>	
				<td><?php  echo "<div style='text-align:center'>".$irasas["uzsakymo_id"]."</div>";?></td>
				<td><?php  echo "<div style='text-align:center'>".$kliento_irasas["vardas"]."</div>";?></td>
				<td><?php  echo "<div style='text-align:center'>".$kliento_irasas["pavarde"]."</div>";?></td>
				<td><?php  echo "<div style='text-align:center'>".$irasas["galutine_suma"]."</div>";?></td>
				<td><?php  echo "<div style='text-align:center'>".$irasas["pristatymo_budas"]."</div>";?></td>
				<form method='post'>	
				<td>
				<select onchange="pasirinktasID(<?php echo $irasas['uzsakymo_id'] ?>), atnaujintiBusena(<?php echo $i ?>)" id='busena' style="width: 120px" >
				<option
				value="">
				</option>
				
				<option <?php if (htmlspecialchars($irasas["uzsakymo_busena"]) == "Pateiktas"): ?>
				selected
				<?php endif; ?>
				value="Pateiktas">Pateiktas
				</option>
				
				<option <?php if (htmlspecialchars($irasas["uzsakymo_busena"]) == "Pradėtas vykdyti"): ?>
				selected
				<?php endif; ?>
				value="Pradėtas vykdyti">Pradėtas vykdyti
				</option>

				<option <?php if (htmlspecialchars($irasas["uzsakymo_busena"]) == "Paruoštas atsiimti"): ?>
				selected
				<?php endif; ?>
				value="Paruoštas atsiimti">Paruoštas atsiimti
				</option>		

				<option <?php if (htmlspecialchars($irasas["uzsakymo_busena"]) == "Įvykdytas"): ?>
				selected
				<?php endif; ?>
				value="Įvykdytas">Įvykdytas
				</option>				
				</select>
				</td>
				<td> <input type='submit' name='ok' value='Atnaujinti' class="mygtukas"> 
				<a href="order_details.php?id=<?php echo $irasas['uzsakymo_id'];?>">Detaliau</a>
				</td>
				</form>
				<?php $i++ ?>
			</tr>	
		<?php
				} 
			}
			else {
		?>
		<tr>
			<td><?php  echo "<div style='text-align:center'>".$irasas["uzsakymo_id"]."</div>";?></td>
			<td><?php  echo "<div style='text-align:center'>".$kliento_irasas["vardas"]."</div>";?></td>
			<td><?php  echo "<div style='text-align:center'>".$kliento_irasas["pavarde"]."</div>";?></td>
			<td><?php  echo "<div style='text-align:center'>".$irasas["galutine_suma"]."</div>";?></td>
			<td><?php  echo "<div style='text-align:center'>".$irasas["pristatymo_budas"]."</div>";?></td>
			<form method='post'>	
			
			<td>
			<select onchange="pasirinktasID(<?php echo $irasas['uzsakymo_id'] ?>), atnaujintiBusena(<?php echo $i ?>)" id='busena' style="width: 120px" >
			<option
			value="">
			</option>
			
			<option <?php if (htmlspecialchars($irasas["uzsakymo_busena"]) == "Pateiktas"): ?>
			selected
			<?php endif; ?>
			value="Pateiktas">Pateiktas
			</option>
			
			<option <?php if (htmlspecialchars($irasas["uzsakymo_busena"]) == "Pradėtas vykdyti"): ?>
			selected
			<?php endif; ?>
			value="Pradėtas vykdyti">Pradėtas vykdyti
			</option>

			<option <?php if (htmlspecialchars($irasas["uzsakymo_busena"]) == "Paruoštas atsiimti"): ?>
			selected
			<?php endif; ?>
			value="Paruoštas atsiimti">Paruoštas atsiimti
			</option>		

			<option <?php if (htmlspecialchars($irasas["uzsakymo_busena"]) == "Įvykdytas"): ?>
			selected
			<?php endif; ?>
			value="Įvykdytas">Įvykdytas
			</option>				
			</select>
			</td>
			<td> <input type='submit' name='ok' value='Atnaujinti' class="mygtukas"> 
			<a href="order_details.php?id=<?php echo $irasas['uzsakymo_id'];?>">Detaliau</a>
			</td>
			</form>
			<?php $i++ ?>
		</tr>
			<?php } ?>
	<?php endwhile; ?>
	
</table>
</div>
</body>
	
</html>
<?php } ?>