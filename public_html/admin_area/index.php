﻿<?php
include("includes/session.php");
//session_start();
if (!$session->logged_in && $_SESSION['level'] != "buhalteris") {
	echo "<script>window.open('login.php?not_admin=Jūs neturite teisių!','_self')</script>";
}
else{
?>

<!DOCTYPE>
<html>
	<head>
		<title>Čia yra admino skyrius</title>
		<link rel="stylesheet" href="styles/style.css" media="all"/>
	</head>
<body>
	<div class="main_wrapper">
		<div id="header"></div>
		<div id="right">
			<h2 style="text-align:center;">Valdyti turinį</h2>
			<a href="index.php?view_customers">Peržiūrėti klientus</a>
			<a href="index.php?view_orders">Peržiūrėti užsakymus</a>
			<a href="index.php?popular_products">Populiariausios prekės</a>
			<a href="index.php?loyal_customers">Lojalūs klientai</a>
			<a href="logout.php">Atsijungti</a>
		</div>
		<div id="left">
		<h2 style="color:red; text-align:center;"><?php echo @$_GET['logged_in']; ?></h2>
			<?php
			if(isset($_GET['view_customers'])){
				include("view_customers.php");
			}
			if(isset($_GET['view_orders'])){
				include("view_orders.php");
			}
			if(isset($_GET['popular_products'])){
				include("popular_products.php");
			}
			if(isset($_GET['loyal_customers'])){
				include("loyal_customers.php");
			}
			?>
			
		</div>
	</div>

</body>

</html>
<?php } ?>