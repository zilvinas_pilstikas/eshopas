﻿<?php

$con = mysqli_connect("stud.if.ktu.lt","zilpil","ush5ei1aeph1af9E","zilpil");
mysqli_set_charset( $con, 'utf8' );
if (mysqli_connect_errno()){
	echo "Nepavyko prisijungti prie MySQL: " . mysqli_connect_error();
}

//Gaunamas vartotojo ip adresas
function getIp(){
	$ip = $_SERVER['REMOTE_ADDR'];
	if(!empty($_SERVER['HTTP_CLIENT_IP'])){
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	return $ip;
}

//Sukuriamas prekiu krepselis
function cart(){
	if(isset($_GET['add_cart'])){
		global $con;
		$c_email = $_SESSION['customer_email'];		//vartotojo pastas
		
		$get_c = "select * from vartotojai where el_pastas='$c_email'";		//paimamas vartotojas
		$run_c = mysqli_query($con,$get_c);
		$row_c = mysqli_fetch_array($run_c);
		$c_id = $row_c['id'];						//paimamas vartotojo id
		
		$pro_id = $_GET['add_cart'];				//produkto bruksninis_kodas
		$get_cart = "select * from prekiu_krepseliai where vartotojo_id='$c_id'";		//paimamas vartotojo krepselis
		$run_cart = mysqli_query($con,$get_cart);
		if(mysqli_num_rows($run_cart)>0){
			echo "";
		}
		else{
			$insert_cart = "insert into prekiu_krepseliai (vartotojo_id,prekiu_kaina,fk_VARTOTOJASid)
							values ('$c_id',0,'$c_id')";
			$run_insert_cart = mysqli_query($con,$insert_cart);				//sukuriamas vartotojo krepselis
		}
		$get_cart_id = "select * from prekiu_krepseliai where vartotojo_id='$c_id'";		//paimamas vartotojo krepselis
		$run_cart_id = mysqli_query($con,$get_cart_id);
		$row_cart = mysqli_fetch_array($run_cart_id);
		$cart_id = $row_cart['krepselio_id'];								//paimamas krepselio id
		$cart_total = $row_cart['prekiu_kaina'];
		
		$get_pro = "select * from kliento_krepselio_prekes where krepselio_id='$cart_id' AND prekes_bruksninis_kodas='$pro_id'";	//patikrina ar vartotojo krepselyje jau yra norima iterpti preke
		$run_pro = mysqli_query($con,$get_pro);
		if(mysqli_num_rows($run_pro)>0){
			echo "<script>alert('Tokia prekė jau yra krepšelyje!')</script>";
		}
		else{
			$sel_prod = "select * from prekes where bruksninis_kodas='$pro_id'";			//paimama preke
			$run_prod = mysqli_query($con,$sel_prod);
			$row_prod = mysqli_fetch_array($run_prod);
			$prod_price = $row_prod['kaina'];						//paimama prekes kaina
			
			$insert_pro="insert into kliento_krepselio_prekes (krepselio_id,prekes_bruksninis_kodas,kiekis,kaina,fk_PREKIU_KREPSELISkrepselio_id,fk_PREKĖbruksninis_kodas) 
						values ('$cart_id','$pro_id',1,'$prod_price','$cart_id','$pro_id')";
			$run_insert=mysqli_query($con,$insert_pro);				//iterpiama preke i vartotojo krepseli
			
			$total = $cart_total + $prod_price;
			
			$update_cart_total = "update prekiu_krepseliai set prekiu_kaina='$total' where vartotojo_id='$c_id'";
			$run_update = mysqli_query($con,$update_cart_total);
			echo "<script>window.open('index.php','_self')</script>";
			
		}
	}
}

//Gaunami visi prideti i krepseli pirkiniai bet ne kiekiai
//pvz jei krepselyje yra acer laptopas kurio kiekis 3
//ir yra stacionarus kompas, kurio kiekis 2
//tai cia grazina kad krepselyje yra du produktai: stacionarus kompas ir laptopas
function total_items(){
	global $con;
	if(isset($_GET['add_cart'])){
		$c_email = $_SESSION['customer_email'];		//vartotojo pastas
		$ip = getIp();
		$get_c = "select * from vartotojai where el_pastas='$c_email'";		//paimamas vartotojas
		$run_c = mysqli_query($con,$get_c);
		$row_c = mysqli_fetch_array($run_c);
		$c_id = $row_c['id'];						//paimamas vartotojo id
	
		$get_cart = "select * from prekiu_krepseliai where vartotojo_id='$c_id'";
		$run_cart = mysqli_query($con,$get_cart);
		$cart_row = mysqli_fetch_array($run_cart);
		$cart_id = $cart_row['krepselio_id'];
		
		$get_prod = "select * from kliento_krepselio_prekes where krepselio_id='$cart_id'";
		$run_prod = mysqli_query($con,$get_prod);
		$prod_row = mysqli_fetch_array($run_prod);
		$count = mysqli_num_rows($run_prod);
		if ($count == 0) {
			$count_items = 0;
		}
		else {
			$count_items = $count;
		}
	} 
	else{
		$c_email = $_SESSION['customer_email'];		//vartotojo pastas
		$ip = getIp();
		$get_c = "select * from vartotojai where el_pastas='$c_email'";		//paimamas vartotojas
		$run_c = mysqli_query($con,$get_c);
		$row_c = mysqli_fetch_array($run_c);
		$c_id = $row_c['id'];						//paimamas vartotojo id
	
		$get_cart = "select * from prekiu_krepseliai where vartotojo_id='$c_id'";
		$run_cart = mysqli_query($con,$get_cart);
		$cart_row = mysqli_fetch_array($run_cart);
		$cart_id = $cart_row['krepselio_id'];
		
		$get_prod = "select * from kliento_krepselio_prekes where krepselio_id='$cart_id'";
		$run_prod = mysqli_query($con,$get_prod);
		$prod_row = mysqli_fetch_array($run_prod);
		$count = mysqli_num_rows($run_prod);
		if ($count == 0) {
			$count_items = 0;
		}
		else {
			$count_items = $count;
		}
	}
	echo $count_items;
}

//Gaunama bendra pirkiniu suma krepselyje
function total_price(){
	$total = 0;
	global $con;
	
	$c_email = $_SESSION['customer_email'];		//vartotojo pastas
	$get_c = "select * from vartotojai where el_pastas='$c_email'";		//paimamas vartotojas
	$run_c = mysqli_query($con,$get_c);
	$row_c = mysqli_fetch_array($run_c);
	$c_id = $row_c['id'];						//paimamas vartotojo id
	
	$get_cart = "select * from prekiu_krepseliai where vartotojo_id='$c_id'";
	$run_cart = mysqli_query($con,$get_cart);
	$cart_row = mysqli_fetch_array($run_cart);
	$cart_id = $cart_row['krepselio_id'];
	
	$get_pro = "select * from kliento_krepselio_prekes where krepselio_id='$cart_id'";
	$run_pro = mysqli_query($con,$get_pro);
	while($pro_row = mysqli_fetch_array($run_pro)){
		$pro_price = $pro_row['kaina'];
		$pro_qty = $pro_row['kiekis'];
		$total = $total + $pro_price * $pro_qty;
	}
	$update_total = "update prekiu_krepseliai set prekiu_kaina='$total' where krepselio_id='$cart_id' AND vartotojo_id='$c_id'";
	$run_tot = mysqli_query($con,$update_total);
	
	echo $total;
}


//Gaunamos kategorijos
function getCats(){
	
	global $con;
	$get_cats = "select distinct kategorija from prekes";
	
	$run_cats = mysqli_query($con,$get_cats);
	
	while($row_cats=mysqli_fetch_array($run_cats)){
		$cat_title = $row_cats['kategorija'];
		
		echo "<li><a href='index.php?cat=$cat_title'>$cat_title</a></li>";
	}
	
}

//Gaunamos rūšys
function getBrands(){
	
	global $con;
	$get_brands = "select distinct gamintojas from prekes";
	
	$run_brands = mysqli_query($con,$get_brands);
	
	while($row_brands=mysqli_fetch_array($run_brands)){
		$brand_title = $row_brands['gamintojas'];
		
		echo "<li><a href='index.php?brand=$brand_title'>$brand_title</a></li>";
	}
	
}

function getPro(){
	
	if(!isset($_GET['cat'])) {
		if(!isset($_GET['brand'])){
	
			global $con;
			$get_pro = "select * from prekes order by  RAND() LIMIT 0,6";
			$run_pro = mysqli_query($con,$get_pro);
			while($row_pro=mysqli_fetch_array($run_pro)){
				$pro_id = $row_pro['bruksninis_kodas'];
				$pro_cat = $row_pro['kategorija'];
				$pro_brand = $row_pro['gamintojas'];
				$pro_title = $row_pro['pavadinimas'];
				$pro_price = $row_pro['kaina'];
				$pro_qty = $row_pro['kiekis'];
				$pro_image = $row_pro['nuotrauka'];
		
				if(!isset($_SESSION['customer_email'])){ 
						echo "
						<div id='single_product'>
							<h3>$pro_title</h3>
							<img src='manager_area/product_images/$pro_image' width='180' height='180' />
							<p><b> \xE2\x82\xAc $pro_price liko: $pro_qty vnt </b></p>
							<a href='details.php?pro_id=$pro_id' style='float:left;'>Detalės</a>
							<a href='checkout.php'><button style='float:right'>Pridėti į krepšelį</button></a>
						</div>
						";
						} else{
						echo "
						<div id='single_product'>
							<h3>$pro_title</h3>
							<img src='manager_area/product_images/$pro_image' width='180' height='180' />
							<p><b> \xE2\x82\xAc $pro_price liko: $pro_qty vnt </b></p>
							<a href='details.php?pro_id=$pro_id' style='float:left;'>Detalės</a>
							<a href='index.php?add_cart=$pro_id'><button style='float:right'>Pridėti į krepšelį</button></a>
						</div>
						";
						}
			}
		}
	}
}

function getCatPro(){
	
	if(isset($_GET['cat'])) {
		
		$cat_id = $_GET['cat'];
	
			global $con;
			$get_cat_pro = "select * from prekes where kategorija='$cat_id'";
			$run_cat_pro = mysqli_query($con,$get_cat_pro);
			$count_cats = mysqli_num_rows($run_cat_pro);
			if($count_cats==0){
				echo"<h2 style='padding:20px;'>Šioje kategorijoje nėra produktų!<h2>";
			}
			while($row_cat_pro=mysqli_fetch_array($run_cat_pro)){
				$pro_id = $row_cat_pro['bruksninis_kodas'];
				$pro_cat = $row_cat_pro['kategorija'];
				$pro_brand = $row_cat_pro['gamintojas'];
				$pro_title = $row_cat_pro['pavadinimas'];
				$pro_price = $row_cat_pro['kaina'];
				$pro_qty = $row_cat_pro['kiekis'];
				$pro_image = $row_cat_pro['nuotrauka'];
	
					if(!isset($_SESSION['customer_email'])){ 
						echo "
						<div id='single_product'>
							<h3>$pro_title</h3>
							<img src='manager_area/product_images/$pro_image' width='180' height='180' />
							<p><b> \xE2\x82\xAc $pro_price liko: $pro_qty vnt </b></p>
							<a href='details.php?pro_id=$pro_id' style='float:left;'>Detalės</a>
							<a href='checkout.php'><button style='float:right'>Pridėti į krepšelį</button></a>
						</div>
						";
						} else{
						echo "
						<div id='single_product'>
							<h3>$pro_title</h3>
							<img src='manager_area/product_images/$pro_image' width='180' height='180' />
							<p><b> \xE2\x82\xAc $pro_price liko: $pro_qty vnt </b></p>
							<a href='details.php?pro_id=$pro_id' style='float:left;'>Detalės</a>
							<a href='index.php?add_cart=$pro_id'><button style='float:right'>Pridėti į krepšelį</button></a>
						</div>
						";
						}
			}
	}
}

function getBrandPro(){
	
	if(isset($_GET['brand'])) {
		
		$brand_id = $_GET['brand'];
	
			global $con;
			$get_brand_pro = "select * from prekes where gamintojas='$brand_id'";
			$run_brand_pro = mysqli_query($con,$get_brand_pro);
			$count_brands = mysqli_num_rows($run_brand_pro);
			if($count_brands==0){
				echo"<h2 style='padding:20px;'>Šios rūšies produktų nėra!<h2>";
			}
			while($row_brand_pro=mysqli_fetch_array($run_brand_pro)){
				$pro_id = $row_brand_pro['bruksninis_kodas'];
				$pro_cat = $row_brand_pro['kategorija'];
				$pro_brand = $row_brand_pro['gamintojas'];
				$pro_title = $row_brand_pro['pavadinimas'];
				$pro_price = $row_brand_pro['kaina'];
				$pro_qty = $row_brand_pro['kiekis'];
				$pro_image = $row_brand_pro['nuotrauka'];
	
					if(!isset($_SESSION['customer_email'])){ 
						echo "
						<div id='single_product'>
							<h3>$pro_title</h3>
							<img src='manager_area/product_images/$pro_image' width='180' height='180' />
							<p><b> \xE2\x82\xAc $pro_price liko: $pro_qty vnt </b></p>
							<a href='details.php?pro_id=$pro_id' style='float:left;'>Detalės</a>
							<a href='checkout.php'><button style='float:right'>Pridėti į krepšelį</button></a>
						</div>
						";
						} else{
						echo "
						<div id='single_product'>
							<h3>$pro_title</h3>
							<img src='manager_area/product_images/$pro_image' width='180' height='180' />
							<p><b> \xE2\x82\xAc $pro_price liko: $pro_qty vnt </b></p>
							<a href='details.php?pro_id=$pro_id' style='float:left;'>Detalės</a>
							<a href='index.php?add_cart=$pro_id'><button style='float:right'>Pridėti į krepšelį</button></a>
						</div>
						";
						}
			}
	}
}

?>