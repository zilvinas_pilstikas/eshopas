<!DOCTYPE>
<?php
session_start();
include("functions/functions.php");


?>
<html>
	<head>
	<meta charset="utf-8">
		<title>Shopshopas elektroninė parduotuvė </title>
			
	<link rel="stylesheet" href="styles/style.css" media="all" />
	</head

	
	
<body>

	<!--Pagrindinis kontaineris prasideda cia -->
	<div class="main_wrapper">
	
		<!--Headeris prasideda cia -->
		<div class="header_wrapper">
			
			<a href="index.php"><img id="logo" src="images/logo.gif" /> </a>
			<img id="banner" src="images/ad_banner.gif" />
		</div>
		<!--Headeris pasibaigia cia -->
		
		<!--Meniu juosta prasideda cia -->
		<div class="menubar">
			
			<ul id="menu">
				<li><a href="index.php">Namai</a></li>
				<li><a href="all_products.php">Visi produktai</a></li>
				
				<?php
				if(!isset($_SESSION['customer_email'])){ ?>
					<li><a href="checkout.php">Mano paskyra</a></li> <?php } else { ?>
					<li><a href="customer/my_account.php">Mano paskyra</a></li>
				<?php } ?>
				
				<?php
				if(!isset($_SESSION['customer_email'])){ ?>
					<li><a href="customer_register.php">Užsiregistruoti</a></li>
				<?php } ?>
				
				<li><a href="cart.php">Krepšelis</a></li>
				<!--<li><a href="#">Kontaktai</a></li> -->
			</ul>
			
			<div id="form">
				<form method="get" action="results.php" enctype="multipart/form-data">
					<input type="text" name="user_query" placeholder="Ieškoti produkto"/>
					<input type="submit" name="search" value="Ieškoti" />
				</form>
			
			</div>
			
		</div>
		<!--Meniu juosta baigiasi cia -->
	
		<!--turinys prasideda cia -->
		<div class="content_wrapper">
		
			<div id="sidebar">
				<div id="sidebar_title">Kategorijos</div>
				
				<ul id="cats">
					<?php getCats(); ?>
				</ul>
			
			
				<div id="sidebar_title">Rūšis</div>
				
				
				<ul id="cats">
					<?php getBrands(); ?>
				</ul>
				
			</div>
	
			<div id="content_area">
			<?php cart(); ?>
				<div id="shopping_cart">
					<span style="float:right; font-size:15px; padding:5px; line-height:40px;">
						<?php 
							if (isset($_SESSION['customer_email'])){
								echo "<b> Sveiki: </b>" . $_SESSION['customer_email'] . "<b style='color:yellow;'> Jūsų</b>";
							}
							else{
								echo "<b>Sveiki svečias:<b/b>";
							}
							//ob_start(); //Start output buffer
							//total_price();
							//$total_p = ob_get_contents(); //Grab output
							//ob_end_clean();
							
							//ob_start(); //Start output buffer
							//total_items();
							//$total_q = ob_get_contents(); //Grab output
							//ob_end_clean();
							
							//$tot = $total_p * $total_q;
							$eur = " \xE2\x82\xAc ";
							
						?>
						<b style="color:yellow">Pirkinių krepšelis - </b> Iš viso produktų: <?php total_items(); ?> Bendra suma: <?php echo total_price() . $eur; ?><a href="index.php" style="color:yellow">Grįžti į pradžią</a>
						<?php
							if(!isset($_SESSION['customer_email'])){
								echo "<a href='checkout.php' style='color:orange'>Prisijungti</a>";
							}
							else {
								echo "<a href='logout.php' style='color:orange'>Atsijungti</a>";
							}
						?>
					</span>
					
					
				</div>
				<div id="products_box">
					<?php getPro(); ?>
					<?php getCatPro(); ?>
					<?php getBrandPro(); ?>
				</div>
			</div>
		</div>
		<!--turinys baigiasi cia -->
	
	
		<div id="footer">
			<h2 style="text-align:center; padding-top:30px;">&copy; 2016 GO TM elektroninė parduotuvė</h2>
		</div>
	
	
	
	</div>
	<!--Pagrindinis kontaineris pasibaigia cia -->

	
</body>
<html>