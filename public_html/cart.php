﻿<!DOCTYPE>
<?php
session_start();
include("functions/functions.php");


?>
<html>
	<head>
	<meta charset="utf-8">
		<title>Shopshopas elektroninė parduotuvė</title>
			
	<link rel="stylesheet" href="styles/style.css" media="all" />
	</head

	
	
<body>

	<!--Pagrindinis kontaineris prasideda cia -->
	<div class="main_wrapper">
	
		<!--Headeris prasideda cia -->
		<div class="header_wrapper">
			
			<a href="index.php"><img id="logo" src="images/logo.gif" /> </a>
			<img id="banner" src="images/ad_banner.gif" />
		</div>
		<!--Headeris pasibaigia cia -->
		
		<!--Meniu juosta prasideda cia -->
		<div class="menubar">
			
			<ul id="menu">
				<li><a href="index.php">Namai</a></li>
				<li><a href="all_products.php">Visi produktai</a></li>
				<?php
				if(!isset($_SESSION['customer_email'])){ ?>
					<li><a href="checkout.php">Mano paskyra</a></li> <?php } else { ?>
					<li><a href="customer/my_account.php">Mano paskyra</a></li>
				<?php } ?>
				
				<?php
				if(!isset($_SESSION['customer_email'])){ ?>
					<li><a href="customer_register.php">Užsiregistruoti</a></li>
				<?php } ?>
				<li><a href="cart.php">Krepšelis</a></li>
				<!--<li><a href="#">Kontaktai</a></li> -->
			</ul>
			
			<div id="form">
				<form method="get" action="results.php" enctype="multipart/form-data">
					<input type="text" name="user_query" placeholder="Ieškoti produkto"/>
					<input type="submit" name="search" value="Ieškoti" />
				</form>
			
			</div>
			
		</div>
		<!--Meniu juosta baigiasi cia -->
	
		<!--turinys prasideda cia -->
		<div class="content_wrapper">
		
			<div id="sidebar">
				<div id="sidebar_title">Kategorijos</div>
				
				<ul id="cats">
					<?php getCats(); ?>
				</ul>
			
			
				<div id="sidebar_title">Rūšis</div>
				
				
				<ul id="cats">
					<?php getBrands(); ?>
				</ul>
				
			</div>
	
			<div id="content_area">
			<?php cart(); ?>
				<div id="shopping_cart">
					<span style="float:right; font-size:15px; padding:5px; line-height:40px;">
						<?php 
							if (isset($_SESSION['customer_email'])){
								echo "<b> Sveiki: </b>" . $_SESSION['customer_email'] . "<b style='color:yellow;'> Jūsų</b>";
							}
							else{
								echo "<b>Sveiki svečias:<b/b>";
							}
							$eur = " \xE2\x82\xAc ";
							
						?>
						<b style="color:yellow">Pirkinių krepšelis - </b> Iš viso produktų: <?php total_items(); ?> Bendra suma: <?php echo total_price() . $eur; ?><a href="index.php" style="color:yellow">Grįžti į pradžią</a>
						<?php
							if(!isset($_SESSION['customer_email'])){
								echo "<a href='checkout.php' style='color:orange'>Prisijungti</a>";
							}
							else {
								echo "<a href='logout.php' style='color:orange'>Atsijungti</a>";
							}
						?>
					</span>
				</div>
				<div id="products_box">
				
					<form action="" method="post" enctype="multipart/form-data">
						<table align="center" width="700" bgcolor="skyblue">
						
							<tr align="center">
								<th>Pašalinti</th>
								<th>Produktas</th>
								<th>Kiekis</th>
								<th>Kaina</th>
							</tr>
							
							<?php 
								global $con;
								
								$c_email = $_SESSION['customer_email'];		//vartotojo pastas
								
								$get_c = "select * from vartotojai where el_pastas='$c_email'";		//paimamas vartotojas
								$run_c = mysqli_query($con,$get_c);
								$row_c = mysqli_fetch_array($run_c);
								$c_id = $row_c['id'];						//paimamas vartotojo id
								
								$get_cart = "select * from prekiu_krepseliai where vartotojo_id='$c_id'";
								$run_cart = mysqli_query($con,$get_cart);
								$cart_row = mysqli_fetch_array($run_cart);
								$cart_id = $cart_row['krepselio_id'];
								$cart_total = $cart_row['prekiu_kaina'];
								
								$get_prod = "select * from kliento_krepselio_prekes where krepselio_id='$cart_id'";
								$run_prod = mysqli_query($con,$get_prod);
								$i=0;
								while($prod_row = mysqli_fetch_array($run_prod)){
									$pro_price = $prod_row['kaina'];
									$pro_qty = $prod_row['kiekis'];
									$pro_bar_code = $prod_row['prekes_bruksninis_kodas'];
									
									$get_title = "select * from prekes where bruksninis_kodas='$pro_bar_code'";
									$run_title = mysqli_query($con,$get_title);
									$title_row = mysqli_fetch_array($run_title);
									$pro_title = $title_row['pavadinimas'];
									$pro_image = $title_row['nuotrauka'];
									
							?>
							
							<tr align="center">
								<td><input type="checkbox" name="remove[]" value="<?php echo $pro_bar_code;?>"/></td>
								<td><?php echo $pro_title; ?><br>
								<img src="manager_area/product_images/<?php echo $pro_image;?>" width="60" height="60"/>
								</td>
								<td><input type="text" size="4" name="qty[]" value="<?php echo $pro_qty;?>" /></td>
								
								<?php
									if(isset($_POST['update_cart'])){
											
										$qty = $_POST['qty'][$i];
										$i++;
										$update_qty = "update kliento_krepselio_prekes set kiekis='$qty' where krepselio_id='$cart_id' AND prekes_bruksninis_kodas='$pro_bar_code'";
										$run_qty = mysqli_query($con,$update_qty);
										
										foreach($_POST['remove'] as $remove_id){
											$delete_product = "delete from kliento_krepselio_prekes where prekes_bruksninis_kodas='$remove_id' AND krepselio_id='$cart_id'";
											$run_delete = mysqli_query($con,$delete_product);
											if($run_delete){
												echo "<script>window.open('cart.php','_self')</script>";
											}
										}
										echo "<script>window.open('cart.php','_self')</script>";
									}
								?>
								<td><?php echo $pro_price . "\xE2\x82\xAc"; ?></td>
							</tr>
							
								<?php } ?>
								
								<tr>
									<td colspan="4" align="right"><b>Bendra Suma:</b></td>		
									<td><?php echo total_price() . " \xE2\x82\xAc";?></td>
								</tr>
								
								<tr align="center">
									<td colspan="2"><input type="submit" name="update_cart" value="Atnaujinti krepšelį"/></td>
									<td><a href = "index.php" style="color:red; font-size:18px;">tęsti apsipirkimą</a></td>
									<td><a href="checkout.php" style="color:red; font-size:18px;">Susimokėti</a></td>
								</tr>
								
							<!--</tr> -->
						</table>
					</form>
						<?php
						function updatecart(){
							global $con;
							if(isset($_POST['continue'])){
								echo "<script>window.open('index.php','_self')</script>";
							}
						}
						echo @$up_cart = updatecart();
						?>
				</div>
			</div>
		</div>
		<!--turinys baigiasi cia -->
	
	
		<div id="footer">
			<h2 style="text-align:center; padding-top:30px;">&copy; 2016 GO TM elektroninė parduotuvė</h2>
		</div>
	
	
	
	</div>
	<!--Pagrindinis kontaineris pasibaigia cia -->

	
</body>
<html>