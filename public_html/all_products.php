<!DOCTYPE>
<?php
session_start();
include("functions/functions.php");


?>
<html>
	<head>
	<meta charset="utf-8">
		<title>Shopshopas elektroninė parduotuvė</title>
			
	<link rel="stylesheet" href="styles/style.css" media="all" />
	</head

	
	
<body>

	<!--Pagrindinis kontaineris prasideda cia -->
	<div class="main_wrapper">
	
		<!--Headeris prasideda cia -->
		<div class="header_wrapper">
			
			<a href="index.php"><img id="logo" src="images/logo.gif" /> </a>
			<img id="banner" src="images/ad_banner.gif" />
		</div>
		<!--Headeris pasibaigia cia -->
		
		<!--Meniu juosta prasideda cia -->
		<div class="menubar">
			
			<ul id="menu">
				<li><a href="index.php">Namai</a></li>
				<li><a href="all_products.php">Visi produktai</a></li>
				<?php
				if(!isset($_SESSION['customer_email'])){ ?>
					<li><a href="checkout.php">Mano paskyra</a></li> <?php } else { ?>
					<li><a href="customer/my_account.php">Mano paskyra</a></li>
				<?php } ?>
				
				<?php
				if(!isset($_SESSION['customer_email'])){ ?>
					<li><a href="customer_register.php">Užsiregistruoti</a></li>
				<?php } ?>
				<li><a href="cart.php">Krepšelis</a></li>
				<!--<li><a href="#">Kontaktai</a></li> -->
			</ul>
			
			<div id="form">
				<form method="get" action="results.php" enctype="multipart/form-data">
					<input type="text" name="user_query" placeholder="Ieškoti produkto"/>
					<input type="submit" name="search" value="Ieškoti" />
				</form>
			
			</div>
			
		</div>
		<!--Meniu juosta baigiasi cia -->
	
		<!--turinys prasideda cia -->
		<div class="content_wrapper">
		
			<div id="sidebar">
				<div id="sidebar_title">Kategorijos</div>
				
				<ul id="cats">
					<?php getCats(); ?>
				</ul>
			
			
				<div id="sidebar_title">Rūšis</div>
				
				
				<ul id="cats">
					<?php getBrands(); ?>
				</ul>
				
			</div>
	
			<div id="content_area">
				<div id="shopping_cart">
					<span style="float:right; font-size:15px; padding:5px; line-height:40px;">
						<?php 
							if (isset($_SESSION['customer_email'])){
								echo "<b> Sveiki: </b>" . $_SESSION['customer_email'] . "<b style='color:yellow;'> Jūsų</b>";
							}
							else{
								echo "<b>Sveiki svečias:<b/b>";
							}
							$eur = " \xE2\x82\xAc ";
							
						?>
						<b style="color:yellow">Pirkinių krepšelis - </b> Iš viso produktų: <?php total_items(); ?> Bendra suma: <?php echo total_price() . $eur; ?><a href="index.php" style="color:yellow">Grįžti į pradžią</a>
						<?php
							if(!isset($_SESSION['customer_email'])){
								echo "<a href='checkout.php' style='color:orange'>Prisijungti</a>";
							}
							else {
								echo "<a href='logout.php' style='color:orange'>Atsijungti</a>";
							}
						?>
					</span>
				</div>
				<div id="products_box">
				<?php 
					$get_pro = "select * from prekes";
					$run_pro = mysqli_query($con,$get_pro);
					while($row_pro=mysqli_fetch_array($run_pro)){
						$pro_id = $row_pro['bruksninis_kodas'];
						$pro_cat = $row_pro['kategorija'];
						$pro_brand = $row_pro['gamintojas'];
						$pro_title = $row_pro['pavadinimas'];
						$pro_price = $row_pro['kaina'];
						$pro_qty = $row_pro['kiekis'];
						$pro_image = $row_pro['nuotrauka'];
		
						if(!isset($_SESSION['customer_email'])){ 
						echo "
						<div id='single_product'>
							<h3>$pro_title</h3>
							<img src='manager_area/product_images/$pro_image' width='180' height='180' />
							<p><b> \xE2\x82\xAc $pro_price liko: $pro_qty vnt </b></p>
							<a href='details.php?pro_id=$pro_id' style='float:left;'>Detalės</a>
							<a href='checkout.php'><button style='float:right'>Pridėti į krepšelį</button></a>
						</div>
						";
						} else{
						echo "
						<div id='single_product'>
							<h3>$pro_title</h3>
							<img src='manager_area/product_images/$pro_image' width='180' height='180' />
							<p><b> \xE2\x82\xAc $pro_price liko: $pro_qty vnt </b></p>
							<a href='details.php?pro_id=$pro_id' style='float:left;'>Detalės</a>
							<a href='index.php?add_cart=$pro_id'><button style='float:right'>Pridėti į krepšelį</button></a>
						</div>
						";
						}
					}
					?>
				</div>
			</div>
		</div>
		<!--turinys baigiasi cia -->
	
	
		<div id="footer">
			<h2 style="text-align:center; padding-top:30px;">&copy; 2016 GO TM elektroninė parduotuvė</h2>
		</div>
	
	
	
	</div>
	<!--Pagrindinis kontaineris pasibaigia cia -->

	
</body>
<html>