<div>
<?php
include("includes/db.php");
?>

<h2 align="center">Susimokėti su Paypal:</h2>
<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" >

<!-- Identify your business so that you can collect the payments. -->
<input type="hidden" name="business" value="sriniv_1293527277_biz@inbox.com">

<!-- Specify a Buy Now button. -->
<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="upload" value="1">

<?php
$i=1;
	global $con;
	
	$c_email = $_SESSION['customer_email'];		//vartotojo pastas
	$get_c = "select * from vartotojai where el_pastas='$c_email'";		//paimamas vartotojas
	$run_c = mysqli_query($con,$get_c);
	$row_c = mysqli_fetch_array($run_c);
	$c_id = $row_c['id'];						//paimamas vartotojo id
	
	$get_cart = "select * from prekiu_krepseliai where vartotojo_id='$c_id'";
	$run_cart = mysqli_query($con,$get_cart);
	$cart_row = mysqli_fetch_array($run_cart);
	$cart_id = $cart_row['krepselio_id'];
	
	$get_prod = "select * from kliento_krepselio_prekes where krepselio_id='$cart_id'";
	$run_prod = mysqli_query($con,$get_prod);
	while($prod_row = mysqli_fetch_array($run_prod)){
		$pro_price = $prod_row['kaina'];
		$pro_qty = $prod_row['kiekis'];
		$pro_bar_code = $prod_row['prekes_bruksninis_kodas'];
									
		$get_title = "select * from prekes where bruksninis_kodas='$pro_bar_code'";
		$run_title = mysqli_query($con,$get_title);
		$title_row = mysqli_fetch_array($run_title);
		$pro_title = $title_row['pavadinimas'];
		$pro_image = $title_row['nuotrauka'];
		?>
		<!-- Specify details about the item that buyers will purchase. -->
	<input type="hidden" name="item_name_<?php echo $i; ?>" value="<?php echo $pro_title;?>">
	<input type="hidden" name="item_number_<?php echo $i; ?>" value="<?php echo $pro_bar_code; ?>">
	<input type="hidden" name="amount_<?php echo $i; ?>" value="<?php echo $pro_price; ?>">
	<input type="hidden" name="quantity_<?php echo $i; ?>" value="<?php echo $pro_qty; ?>">
	<input type="hidden" name="currency_code" value="USD">
	<?php
	$i++; 
	}
?>

<!-- Specify the Pages for Successful payment & failed Payment-->

<input type="hidden" name="return" value="http://zilpil.stud.if.ktu.lt/paypal_success.php"/>

<input type="hidden" name="cancel_return" value="http://zilpil.stud.if.ktu.lt/paypal_cancel.php"/>

<!-- Display the payment button. -->
<input type="image" name="submit" border="0" src="images/paynow_button.png" alt="PayPal – The safer, easier way to pay online">
<img alt="" border="0" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >

</form>

</div>