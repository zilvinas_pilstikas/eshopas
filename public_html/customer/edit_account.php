					<?php
					include("includes/db.php");
					$user = $_SESSION['customer_email'];
					$get_customer = "select * from vartotojai where el_pastas='$user'";
					$run_customer = mysqli_query($con,$get_customer);
					$row_customer = mysqli_fetch_array($run_customer);
					$c_id = $row_customer['id'];
					$email = $row_customer['el_pastas'];
					$pass = $row_customer['slaptazodis'];
					$group = $row_customer['grupe'];
					$name = $row_customer['vardas'];
					$surname = $row_customer['pavarde'];
					$birth = $row_customer['gimimo_data'];
					$gender = $row_customer['lytis'];
					$mobile = $row_customer['telefonas'];
					$city = $row_customer['miestas'];
					$address = $row_customer['adresas'];
					$certificate = $row_customer['sertifikato_tipas'];
					$news_email = $row_customer['naujienos_el_pastu'];
					$news_mobile = $row_customer['naujienos_telefonu'];
					$create_date = $row_customer['sukurimo_data'];
					$update_date = $row_customer['atnaujinimo_data'];
					$bonus_money = $row_customer['sukaupti_pinigai'];
					$time_zone = $row_customer['laiko_zyma'];
					
					if ($certificate == "juridinis"){
						$get_company = "select * from juridinio_asmens_informacijos where fk_VARTOTOJASid='$c_id'";
						$run_company = mysqli_query($con,$get_company);
						$row_company = mysqli_fetch_array($run_company);
						$company_id = $row_company['id'];
						$company_name = $row_company['imonės_pavadinimas'];
						$company_code = $row_company['imonės_kodas'];
						$company_city = $row_company['miestas'];
						$company_address = $row_company['adresas'];
						$company_pvm_code = $row_company['imonės_PVM_kodas'];
					}
					?>
					
					<form action="" method="post" enctype="multipart/form-data">
					<table align="center" width="750">
						<tr align="center">
							<td colspan="6"><h2>Atnaujinti paskyrą<h2></td>
						</tr>
						
						<tr>
							<td align="right">Vardas:</td>
							<td><input type="text" name="c_name" value="<?php echo $name; ?>" required /></td>
						</tr>
						
						<tr>
							<td align="right">Pavardė:</td>
							<td><input type="text" name="c_surname" value="<?php echo $surname; ?>" required /></td>
						</tr>
						
						<tr>
							<td align="right">E-paštas:</td>
							<td><input type="text" name="c_email" value="<?php echo $email; ?>" disabled /></td>
						</tr>
						
						<tr>
							<td align="right">Grupė:</td>
							<td><input type="text" name="c_group" value="<?php echo $group; ?>" disabled /></td>
						</tr>
						
						<tr>
							<td align="right">Gimimo data:</td>
							<td><input type="date" name="c_birth" value="<?php echo $birth; ?>"required /></td>
						</tr>
						
						<tr>
							<td align="right">Lytis:</td>
							<td>
								<select name="c_gender" disabled>
									<option><?php echo $gender; ?></option>
									<option>Vyras</option>
									<option>Moteris</option>
									<option>Kitas</option>
								</select>
							</td>
						</tr>
						
						<tr>
							<td align="right">Telefonas:</td>
							<td><input type="text" name="c_mobile" value="<?php echo $mobile; ?>"required /></td>
						</tr>
						
						
						<tr>
							<td align="right">Miestas:</td>
							<td>
								<select name="c_city" disabled>
									<option><?php echo $city; ?></option>
									<option>Vilnius</option>
									<option>Kaunas</option>
									<option>Klaipeda</option>
									<option>Panevėžys</option>
									<option>Šiauliai</option>
									<option>Alytus</option>
									<option>Kėdainiai</option>
									<option>Kitas</option>
								</select>
							</td>
						</tr>
								
						<tr>
							<td align="right">Adresas:</td>
							<td><input type="text" name="c_address" value="<?php echo $address; ?>" required /></td>
						</tr>
						
						<tr>
							<td align="right">Sertifikato tipas:</td>
							<td><input type="text" name="c_certificate" value="<?php echo $certificate; ?>" disabled /></td>
						</tr>
						
						<?php if ($certificate == "juridinis"){ ?>
						
						<tr>
							<td align="right">Įmonės pavadinimas:</td>
							<td><input type="text" name="comp_name" value="<?php echo $company_name; ?>" required /></td>
						</tr>
						
						<tr>
							<td align="right">Įmonės kodas:</td>
							<td><input type="text" name="comp_code" value="<?php echo $company_code; ?>" required /></td>
						</tr>
						
						<tr>
							<td align="right">Miestas:</td>
							<td><input type="text" name="comp_city" value="<?php echo $company_city; ?>" required /></td>
						</tr>
						
						<tr>
							<td align="right">Adresas:</td>
							<td><input type="text" name="comp_address" value="<?php echo $company_address; ?>" required /></td>
						</tr>
						
						<tr>
							<td align="right">Įmonės PVM kodas:</td>
							<td><input type="text" name="comp_pvm_code" value="<?php echo $company_pvm_code; ?>" required /></td>
						</tr>
						
						<?php } ?>
						
						<tr>
							<td align="right">Ar norite gauti naujienas el_paštu?</td>
							<td><input type="checkbox" name="c_news_email" value=1 /></td>
						</tr>
						
						<tr>
							<td align="right">Ar norite gauti naujienas telefonu?</td>
							<td><input type="checkbox" name="c_news_mobile" value=1 /></td>
						</tr>
						
						<tr>
							<td align="right">Paskyros sukūrimo data:</td>
							<td><input type="date" name="c_create_date" value="<?php echo $create_date; ?>" disabled /></td>
						</tr>
						
						<tr>
							<td align="right">Paskyros atnaujinimo data:</td>
							<td><input type="date" name="c_upade_date" value="<?php echo $update_date; ?>" disabled /></td>
						</tr>
						
						<tr>
							<td align="right">Sukaupti bonus pinigai:</td>
							<td><input type="double" name="c_bonus_money" value="<?php echo $bonus_money; ?>" disabled /></td>
						</tr>
						
						<tr>
							<td align="right">Laiko žyma:</td>
							<td><input type="int" name="c_time_zone" value="<?php echo $time_zone; ?>" required /></td>
						</tr>
						
						<tr align="center">
							<td colspan="6"><input type="submit" name="update" value="Atnaujinti paskyrą"/></td>
						</tr>
						
					</table>
					</form>

<?php
	if(isset($_POST['update'])){
		$customer_id = $c_id;;
		$c_name = $_POST['c_name'];
		$c_surname = $_POST['c_surname'];
		$c_birth = $_POST['c_birth'];
		$c_mobile = $_POST['c_mobile'];
		$c_address = $_POST['c_address'];
		$c_news_email = $_POST['c_news_email'];
		$c_news_mobile = $_POST['c_news_mobile'];
		$c_time_zone = $_POST['c_time_zone'];
		
		if ($certificate == "juridinis"){
			$comp_name = $_POST['comp_name'];
			$comp_code = $_POST['comp_code'];
			$comp_city = $_POST['comp_city'];
			$comp_address = $_POST['comp_address'];
			$comp_pvm_code = $_POST['comp_pvm_code'];
			
			$update_comp = "update juridinio_asmens_informacijos set imonės_pavadinimas='$comp_name',imonės_kodas='$comp_code',miestas='$comp_city',adresas='$comp_address',
							imonės_PVM_kodas='$comp_pvm_code' where fk_VARTOTOJASid='$customer_id'";
			$run_comp_update = mysqli_query($con,$update_comp);
			if($run_comp_update){
				echo "<script>alert('Jūsų įmonė sėkmingai atnaujinta')</script>";
			}
		}
		
		$update_c = "update vartotojai set vardas='$c_name',pavarde='$c_surname',gimimo_data='$c_birth',telefonas='$c_mobile',adresas='$c_address',
						naujienos_el_pastu='$c_news_email',naujienos_telefonu='$c_news_mobile',laiko_zyma='$c_time_zone',atnaujinimo_data=NOW() where id='$customer_id'";
		
		$run_update = mysqli_query($con,$update_c);
		if($run_update){
			echo "<script>alert('Jūsų paskyra sėkmingai atnaujinta')</script>";
			echo "<script>window.open('my_account.php','_self')</script>";
		}
	}
?>