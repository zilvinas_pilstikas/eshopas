<h2 style="text-align:center;">Keisti slaptažodį</h2>
<form action="" method="post">
	<table align="center" width="600">
		<tr>
			<td align="right"><b>Įveskite dabartinį slaptažodį:</b></td>
			<td><input type="password" name="current_pass" required><br></td>
		</tr>
		<tr>
			<td align="right"><b>Įveskite naują slaptažodį:</b></td>
			<td><input type="password" name="new_pass" required><br></td>
		</tr>
		<tr>
			<td align="right"><b>Patvirtinkite naują slaptažodį:</b></td>
			<td><input type="password" name="new_pass_again" required></td>
		</tr>
		<tr align="center">
			<td colspan="3"><input type="submit" name="change_pass" value="Pakeisti slaptažodį"/></td>
		</tr>
	</table>
</form>
<?php
include("includes/db.php");
if(isset($_POST['change_pass'])){
	$user = $_SESSION['customer_email'];
	$current_pass = $_POST['current_pass'];
	$new_pass = $_POST['new_pass'];
	$new_again = $_POST['new_pass_again'];
	
	$sel_pass = "select * from vartotojai where slaptazodis='$current_pass' AND el_pastas='$user'";
	$run_pass = mysqli_query($con,$sel_pass);
	$check_pass = mysqli_num_rows($run_pass);
	if($check_pass==0){
		echo "<script>alert('Dabartinis slaptažodis yra neteisingas!')</script>";
		exit();
	}
	if ($new_pass != $new_again){
		echo "<script>alert('Neteisingai pakartotas naujas slaptažodis!')</script>";
		exit();
	}
	else{
		$update_pass = "update vartotojai set slaptazodis='$new_pass' where el_pastas='$user'";
		$run_update = mysqli_query($con,$update_pass);
		echo "<script>alert('Slaptažodis sėkmingai atnaujintas!')</script>";
		echo "<script><window.open('my_account.php','_self')</script>";
	}
}
?>