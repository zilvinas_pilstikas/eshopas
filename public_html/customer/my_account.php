<!DOCTYPE>
<?php
session_start();
include("functions/functions.php");


?>
<html>
	<head>
	<meta charset="utf-8">
		<title>Shopshopas elektroninė parduotuvė</title>
			
	<link rel="stylesheet" href="styles/style.css" media="all" />
	</head

	
	
<body>

	<!--Pagrindinis kontaineris prasideda cia -->
	<div class="main_wrapper">
	
		<!--Headeris prasideda cia -->
		<div class="header_wrapper">
			
			<a href="../index.php"><img id="logo" src="images/logo.gif" /> </a>
			<img id="banner" src="images/ad_banner.gif" />
		</div>
		<!--Headeris pasibaigia cia -->
		
		<!--Meniu juosta prasideda cia -->
		<div class="menubar">
			
			<ul id="menu">
				<li><a href="../index.php">Namai</a></li>
				<li><a href="../all_products.php">Visi produktai</a></li>
				<?php
				if(!isset($_SESSION['customer_email'])){ ?>
					<li><a href="checkout.php">Mano paskyra</a></li> <?php } else { ?>
					<li><a href="my_account.php">Mano paskyra</a></li>
				<?php } ?>
				
				<?php
				if(!isset($_SESSION['customer_email'])){ ?>
					<li><a href="../customer_register.php">Užsiregistruoti</a></li>
				<?php } ?>
				<li><a href="../cart.php">Krepšelis</a></li>
				<!--<li><a href="#">Kontaktai</a></li> -->
			</ul>
			
			<div id="form">
				<form method="get" action="results.php" enctype="multipart/form-data">
					<input type="text" name="user_query" placeholder="Ieškoti produkto"/>
					<input type="submit" name="search" value="Ieškoti" />
				</form>
			
			</div>
			
		</div>
		<!--Meniu juosta baigiasi cia -->
	
		<!--turinys prasideda cia -->
		<div class="content_wrapper">
		
			<div id="sidebar">
				<div id="sidebar_title">Mano paskyra</div>
				
				<ul id="cats">
				<?php
					$user = $_SESSION['customer_email'];
					$get_customer = "select * from vartotojai where el_pastas='$user'";
					$run_customer = mysqli_query($con,$get_customer);
					$row_customer = mysqli_fetch_array($run_customer);
					$c_name = $row_customer['vardas'];
				?>
					<li><a href="my_account.php?my_orders">Mano užsakymai</a></li>
					<li><a href="my_account.php?edit_account">Redaguoti paskyrą</a></li>
					<li><a href="my_account.php?change_pass">Pakeisti slaptažodį</a></li>
					<li><a href="my_account.php?delete_account">Ištrinti paskyrą</a></li>
					<li><a href="logout.php">Atsijungti</a></li>
				</ul>
					
				
			</div>
	
			<div id="content_area">
			<?php cart(); ?>
				<div id="shopping_cart">
					<span style="float:right; font-size:15px; padding:5px; line-height:40px;">
						<?php 
							if (isset($_SESSION['customer_email'])){
								echo "<b> Sveiki: </b>" . $_SESSION['customer_email'];
							}
						?>
						
						<?php
							if(!isset($_SESSION['customer_email'])){
								echo "<a href='checkout.php' style='color:orange'>Prisijungti</a>";
							}
							else {
								echo "<a href='logout.php' style='color:orange'>Atsijungti</a>";
							}
						?>
					</span>
					
					
				</div>
				<div id="products_box">
					
					<?php
					if(!isset($_GET['my_orders'])){
						if(!isset($_GET['edit_account'])){
							if(!isset($_GET['change_pass'])){
								if(!isset($_GET['delete_account'])){
					echo "<h2 style='padding:20px;'>Sveiki: $c_name <h2>
					<b>Jūs galite matyti užsakymų istoriją, paspaudus <a href='my_account.php?my_orders'>čia</a></b>";
								}
							}
						}
					}
					?>
					
					<?php
					if(isset($_GET['edit_account'])){
						include("edit_account.php");
					}
					if(isset($_GET['change_pass'])){
						include("change_pass.php");
					}
					if(isset($_GET['delete_account'])){
						include("delete_account.php");
					}
					if(isset($_GET['my_orders'])){
						include("my_orders.php");
					}
					?>
				</div>
			</div>
		</div>
		<!--turinys baigiasi cia -->
	
	
		<div id="footer">
			<h2 style="text-align:center; padding-top:30px;">&copy; 2016 GO TM elektroninė parduotuvė</h2>
		</div>
	
	
	
	</div>
	<!--Pagrindinis kontaineris pasibaigia cia -->

	
</body>
<html>